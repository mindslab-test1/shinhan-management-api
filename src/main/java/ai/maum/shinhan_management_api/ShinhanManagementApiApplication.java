package ai.maum.shinhan_management_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ShinhanManagementApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShinhanManagementApiApplication.class, args);
	}

}
