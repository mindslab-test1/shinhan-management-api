package ai.maum.shinhan_management_api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class InvalidSearchKeyException extends BaseException{
    private final HttpStatus statusCode;
    private final String message;

    public InvalidSearchKeyException(HttpStatus statusCode, String message){
        super(statusCode, "Wrong search key", message);
        this.statusCode = statusCode;
        this.message = message;
    }
}

