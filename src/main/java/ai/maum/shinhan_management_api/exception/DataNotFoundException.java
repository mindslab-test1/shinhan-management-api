package ai.maum.shinhan_management_api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class DataNotFoundException extends BaseException{
    private final HttpStatus statusCode;
    private final String message;

    public DataNotFoundException(HttpStatus statusCode, String message){
        super(statusCode, "Date not found", message);
        this.statusCode = statusCode;
        this.message = message;
    }
}
