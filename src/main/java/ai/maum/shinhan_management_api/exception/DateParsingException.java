package ai.maum.shinhan_management_api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class DateParsingException  extends BaseException{
    private final HttpStatus statusCode;
    private final String message;

    public DateParsingException(HttpStatus statusCode, String message){
        super(statusCode, "Date parsing error", message);
        this.statusCode = statusCode;
        this.message = message;
    }
}
