package ai.maum.shinhan_management_api.exception;

public class ConciergeStandbyFileExistException extends RuntimeException {
    public ConciergeStandbyFileExistException(){
        super("컨시어지 대기화면 동일한 파일명");
    }
}
