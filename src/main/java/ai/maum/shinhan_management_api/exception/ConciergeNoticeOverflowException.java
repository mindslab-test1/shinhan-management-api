package ai.maum.shinhan_management_api.exception;

public class ConciergeNoticeOverflowException extends RuntimeException {
    public ConciergeNoticeOverflowException(){
        super("컨시어지 공지사항 최대 개수 초과");
    }
}
