package ai.maum.shinhan_management_api.common;

import ai.maum.shinhan_management_api.api.human.dto.PageResponseDto;

public class Common {
    public enum HumanSearchKey {
        CODE {
            @Override
            public String toString() {
                return "code";
            }
        },
        BRANCH_NAME {
            @Override
            public String toString() {
                return "branchName";
            }
        },
        DEVICE_ID {
            @Override
            public String toString() {
                return "deviceId";
            }
        },
        MANAGER {
            @Override
            public String toString() {
                return "manager";
            }
        }
    }

    public static int WAITING_TO_CREATED = 0;
    public static int CREATING = 1;
    public static int CREATED = 2;
    public static int WAITING_TO_DISTRIBUTION = 3;
    public static int START_DISTRIBUTION = 4;
    public static int DISTRIBUTING = 5;
    public static int DISTRIBUTED = 6;

    public static PageResponseDto getTotalPage(Long len){
        PageResponseDto pageResponseDto = new PageResponseDto();

        if(len == 0) {
            pageResponseDto.setPageCnt(1);
            return pageResponseDto;
        }
        else {
            int totalPage = (int) (Math.ceil((double) len / 10));
            pageResponseDto.setPageCnt(totalPage);
            return pageResponseDto;
        }
    }

    public static final int AIHUMAN_BRANCH_PAGE = 10;

    public static final int CONCIERGE_FILE_BUFFER_SIZE = 1024;
    public static final int CONCIERGE_STANDBY_MAX = 5;
    public static final int CONCIERGE_STANDBY_LIST_PER_PAGE = 10;
    public static final int CONCIERGE_NOTICE_BRANCH_MAX = 5;
    public static final int CONCIERGE_NOTICE_LIST_PER_PAGE = 10;
}

