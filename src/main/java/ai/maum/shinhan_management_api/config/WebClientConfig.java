package ai.maum.shinhan_management_api.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Configuration
public class WebClientConfig {


    @Value("${url.caching_server}")
    private String cachingServerUrl;

    @Bean
    public WebClient cachingServerWebClient(){
        return WebClient.builder()
                .baseUrl(cachingServerUrl)
               // .filter(this::sessionToken)
                .build();
    }


    private Mono<ClientResponse> sessionToken(ClientRequest request, ExchangeFunction ex) {
        //auth --> basic or oauth
        ClientRequest clientRequest = request.attribute("auth")
                .map(v -> tokenOAuth(request))
                .orElse(request);
        return ex.exchange(clientRequest);
    }

    private ClientRequest tokenOAuth(ClientRequest request){
        return ClientRequest.from(request)
                .headers(h -> h.setBearerAuth("token"))
                .build();
    }
}
