package ai.maum.shinhan_management_api.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
@Builder
public class VideoDistributionGroup {
    @Id
    private int id;
    private int device;
    private int cnt;
    private int totalCnt;
    /**
     * 0: 생성 대기, 1: 생성 진행중, 2: 생성 완료, 3:배포 대기, 4:배포 시작, 5:배포 진행중, 6:배포 완료
     * */
    private int status;
    private LocalDateTime lastDate;
    private LocalDateTime created;
}
