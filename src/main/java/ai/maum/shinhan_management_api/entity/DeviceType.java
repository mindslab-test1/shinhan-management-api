package ai.maum.shinhan_management_api.entity;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Data
@ToString
@Builder
@Table("device_type")
public class DeviceType {
    @Id
    private int id;
    private int resolution;
    private String displayName;
    private String displayImage;
    private String code;
    private LocalDateTime created;
}
