package ai.maum.shinhan_management_api.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
@Builder
public class ConciergeTooltip {
    @Id
    private int id;
    private String text;
    private LocalDateTime created;
}
