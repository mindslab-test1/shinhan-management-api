package ai.maum.shinhan_management_api.entity;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;

@Data
@ToString
@Builder
@Table("scenario")
public class Scenario {
    @Id
    Integer id;
    Integer scenarioVersion;
    Integer scenarioDelegate;
    Integer deviceType;
    Integer gesture;
    String utter;
    String intent;
    Timestamp created;
}
