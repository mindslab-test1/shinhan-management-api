package ai.maum.shinhan_management_api.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
@Builder(toBuilder = true)
public class Device {
    @Id
    private int id;
    private int deviceType;
    private String deviceNo;
    private String branchNo;
    private int scenarioVersion;
    private int aiHuman;
    private String aiHumanUuid;
    private int registerStatus;
    private LocalDateTime created;
}
