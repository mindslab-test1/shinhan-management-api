package ai.maum.shinhan_management_api.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
@Builder
public class ConciergeBranchGuide {

    @Id
    private int id;
    private Integer device;
    private int floor;
    private String image;
    private LocalDateTime created;

}
