package ai.maum.shinhan_management_api.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.sql.Timestamp;

@Data
@Builder
public class Engine {
    @Id
    private int id;
    private String ttsHost;
    private int ttsPort;
    private String ttsPath;
    private String w2lHost;
    private int w2lPort;
    private String displayName;
    private String w2lPath;
    private String version;
    private Timestamp created;
}
