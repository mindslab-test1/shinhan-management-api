package ai.maum.shinhan_management_api.entity;


import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@ToString
@Builder
@Table("ai_human_preview")
public class AiHumanPreview {
    @Id
    Integer id;
    Integer avatar;
    Integer outfit;
    Integer background;
    String fileName;
}
