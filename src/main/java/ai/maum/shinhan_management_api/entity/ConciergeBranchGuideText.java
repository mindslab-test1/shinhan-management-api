package ai.maum.shinhan_management_api.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
@Builder
public class ConciergeBranchGuideText {

    @Id
    private int id;
    private Integer conciergeBranchGuide;
    private String title;
    private String content;
    private LocalDateTime created;
}
