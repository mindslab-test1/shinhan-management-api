package ai.maum.shinhan_management_api.entity;


import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;

@Data
@ToString
@Builder
@Table("branch")
public class Branch {
    @Id
    Integer id;
    String branchNo;
    String branchName;
    String branchAddress;
    Timestamp created;
}
