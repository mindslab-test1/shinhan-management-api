package ai.maum.shinhan_management_api.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
@Builder
public class ConciergeStandby {
    @Id
    private int id;
    private int configId;
    private int fileId;
    private int textR;
    private int textG;
    private int textB;
    private int position;
    private LocalDateTime created;
}
