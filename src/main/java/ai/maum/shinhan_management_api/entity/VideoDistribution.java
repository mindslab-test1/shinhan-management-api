package ai.maum.shinhan_management_api.entity;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Data
@ToString
@Builder
@Table("video_distribution")
public class VideoDistribution {
    @Id
    Integer id;
    Integer video;
    Integer groupId;
    /**
     * 0: 생성 대기, 1: 생성 진행중, 2: 생성 완료, 3:배포 대기, 4:배포 시작, 5:배포 진행중, 6:배포 완료
     * */
    Integer status;
    Integer scenarioVersion;
    LocalDateTime created;
}
