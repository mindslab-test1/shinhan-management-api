package ai.maum.shinhan_management_api.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
@Builder
public class ConciergeStandbyFile {
    @Id
    private int id;
    private String name;
    private String imageYn;
    private String file;
    private int size;
    private LocalDateTime created;
}
