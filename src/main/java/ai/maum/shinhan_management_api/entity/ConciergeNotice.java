package ai.maum.shinhan_management_api.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
public class ConciergeNotice {
    @Id
    private int id;
    private Integer device;
    private String text;
    private int position;
    private String branchYn;
    private LocalDate startDate;
    private LocalDate endDate;
    private String useYn;
    private LocalDateTime created;
}
