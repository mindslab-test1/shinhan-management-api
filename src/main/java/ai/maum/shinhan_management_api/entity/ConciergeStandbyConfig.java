package ai.maum.shinhan_management_api.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
public class ConciergeStandbyConfig {
    @Id
    private int id;
    private int device;
    private boolean typeA;
    private boolean typeB;
    private boolean typeC;
    private LocalDateTime created;
}
