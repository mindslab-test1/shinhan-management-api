package ai.maum.shinhan_management_api.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import reactor.util.annotation.Nullable;

import java.time.LocalDateTime;
@Data
@Builder
public class Video {
    @Id
    private int id;
    private int engine;
    private int avatar;
    private int outfit;
    private int background;
    private int resolution;
    private int gesture;
    private int scenario;
    private int scenarioVersion;
    private String name;
    private Long size;
    private LocalDateTime created;
}

