package ai.maum.shinhan_management_api.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
@Builder
public class AiHuman {
    @Id
    private int id;
    private String uuid;
    private int device;
    private int engine;
    private int avatar;
    private int outfit;
    private int background;
    private int resolution;
    private int scenarioVersion;
    private LocalDateTime created;
}
