package ai.maum.shinhan_management_api.entity;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;

@Data
@ToString
@Builder
@Table("background")
public class Background {
    @Id
    Integer id;
    String displayName;
    String imageUrl;
    Timestamp created;
}
