package ai.maum.shinhan_management_api.entity;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;

@Data
@ToString
@Builder
@Table("outfit")
public class Outfit {
    @Id
    Integer id;
    Integer avatar;
    Integer engine;
    String displayName;
    String displayImage;
    Timestamp created;
}
