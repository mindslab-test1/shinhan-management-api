package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.ConciergeStandbyConfig;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface ConciergeStandbyConfigRepository extends ReactiveCrudRepository<ConciergeStandbyConfig, Integer> {
    Mono<ConciergeStandbyConfig> findByDevice(int device);
}
