package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.Device;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface DeviceRepository extends ReactiveCrudRepository<Device, Integer> {
    @Query("select * from device d where d.device_type = :deviceType")
    Flux<Device> findByDeviceType(int deviceType);
    Mono<Device> findTopByDeviceNoAndBranchNoOrderByCreatedAsc(String deviceNo, String branchNo);
    Mono<Device> findTopByBranchNoAndDeviceNoOrderByCreatedAsc(String branchNo, String deviceNo);
    Mono<Device> findByBranchNoAndDeviceNoAndDeviceType(String branchNo, String deviceNo, int deviceType);
    Flux<Device> findByBranchNoAndDeviceType(String branchNo, int deviceType);
    Flux<Device> findByDeviceTypeLike(int deviceType, PageRequest pageRequest);
    @Query("select * from device where id between :startId and :endId")
    Flux<Device> findByIdInRange(int startId, int endId);
    Mono<Device> findByBranchNoAndDeviceNo(String branchNo, String deviceNo);
    Flux<Device> findByDeviceTypeOrderByCreatedDescIdDesc(int deviceType, PageRequest pageRequest);
}
