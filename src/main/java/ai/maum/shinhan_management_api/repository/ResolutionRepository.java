package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.Resolution;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface ResolutionRepository extends ReactiveCrudRepository<Resolution, Integer> {
    Mono<Resolution> findTopById(int id);
}
