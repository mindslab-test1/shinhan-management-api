package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.VideoDistributionGroup;
import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface VideoDistributionGroupRepo extends ReactiveCrudRepository<VideoDistributionGroup, Integer> {

    Flux<VideoDistributionGroup> findAllByStatusIsNotLikeOrderById(int status);

    Flux<VideoDistributionGroup> findAllByStatusIsLike(int status);

    Flux<VideoDistributionGroup> findAllById(int id);

    @Query(value = "select * from video_distribution_group where status >=:start and status <=:end")
    Flux<VideoDistributionGroup> findAllByStatusBetween(int start, int end);


    Flux<VideoDistributionGroup> findAllByDeviceAndStatusEquals(int deviceId, int status);

    @Query(value =
            "UPDATE video_distribution_group SET status=4  WHERE id =:groupId AND status=3")
    Mono<Void> updateVideoByStatusAndGroupId (int groupId);


    @Query(value = "select count(*) from video_distribution_group where status=1")
    Mono<Integer> countStatusValue();

    @Query(value = "select * from video_distribution_group vdg " +
            "left join device d on vdg.device = d.id " +
            "where d.device_type = :deviceType and vdg.status = :status")
    Flux<VideoDistributionGroup> findAllByDeviceTypeAndStatus(int deviceType, int status);

    Flux<VideoDistributionGroup> findAllByStatusLessThan(int status);

    @Query( value =
            "select video_distribution_group.id from video_distribution_group "+
            "left join device de on video_distribution_group.device  = de.id "+
            "where video_distribution_group.status = 4 and "+
            "de.device_no= :deviceNo and de.branch_no= :branchNo")
    Mono<Integer> findGroupIdByDeviceNoAndBranchNo (String deviceNo, String branchNo);

    @Modifying
    @Query("update video_distribution_group set status = 1 where id = :id and status = 0;")
    Mono<Integer> updateStatusProgressCreateById(int id);

    @Query("update video_distribution_group " +
            "set cnt = cnt + 1, " +
            "status = case " +
            "when cnt = 1 then 1 " +
            "when cnt = total_cnt then 2 " +
            "else status " +
            "end " +
            "where id = :id and status < 2;")
    Mono<Void> updateCreatedCntAndStatusById(int id);

    @Query("select d.device_type from video_distribution_group vdg " +
            "left join device d on vdg.device = d.id " +
            "where vdg.id = :id")
    Mono<Integer> findDeviceTypeById(int id);

    @Modifying
    @Query("update video_distribution_group " +
            "set cnt = cnt + 1, " +
            "status = case " +
            "when cnt = 1 then 5 " +
            "when cnt = total_cnt then 6 " +
            "else status " +
            "end " +
            "where id = :id and status >= 4 and status < 6;")
    Mono<Integer> updateReleasedCntAndStatusById(int id);
}
