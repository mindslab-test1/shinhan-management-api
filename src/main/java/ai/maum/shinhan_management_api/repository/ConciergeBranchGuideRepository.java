package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.ConciergeBranchGuide;
import ai.maum.shinhan_management_api.entity.ConciergeNotice;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface ConciergeBranchGuideRepository extends ReactiveCrudRepository<ConciergeBranchGuide, Integer> {

    Flux<ConciergeBranchGuide> findByDevice(int device);
    @Query("select * from concierge_branch_guide where device = :device order by floor")
    Flux<ConciergeBranchGuide> findFloorSort(int device);
    Flux<ConciergeBranchGuide> findByOrderByCreatedDescId(PageRequest pageRequest);


}
