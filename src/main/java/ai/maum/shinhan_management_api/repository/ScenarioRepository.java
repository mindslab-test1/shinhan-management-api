package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.Scenario;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ScenarioRepository extends ReactiveCrudRepository<Scenario, Integer> {
    Mono<Scenario> findById(int id);

    // 나중에는 Gesture 일치시킨다
    // Mono<Scenario> findByScenarioVersionAndDeviceTypeAndUtterAndGesture
    Mono<Scenario> findTopByScenarioVersionAndDeviceTypeAndUtterOrderByCreatedAsc(int scenarioVersion, int deviceType, String utter);
}
