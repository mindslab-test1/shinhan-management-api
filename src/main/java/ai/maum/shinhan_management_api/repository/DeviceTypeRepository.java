package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.DeviceType;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface DeviceTypeRepository extends ReactiveCrudRepository<DeviceType, Integer> {

    Mono<DeviceType> findById(int id);

    Mono<DeviceType> findTopById(int deviceType);

    Mono<DeviceType> findByCode(String code);
}
