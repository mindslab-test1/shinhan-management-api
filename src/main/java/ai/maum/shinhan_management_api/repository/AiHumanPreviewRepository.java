package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.AiHumanPreview;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Mono;

public interface AiHumanPreviewRepository extends R2dbcRepository<AiHumanPreview,Integer> {

    @Query("SELECT file_name FROM ai_human_preview WHERE avatar=:avatar and outfit=:outfit and background=:background and preiview_num:previewNum;")
    Mono<AiHumanPreview> findPreviewFileName (int avatar, int outfit, int background, int previewNum);
}
