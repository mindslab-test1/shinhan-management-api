package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.ScenarioVersion;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface ScenarioVersionRepository extends ReactiveCrudRepository<ScenarioVersion, Integer> {
    Mono<ScenarioVersion> findTopById(int id);
}
