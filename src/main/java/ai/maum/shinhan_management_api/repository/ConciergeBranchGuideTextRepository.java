package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.ConciergeBranchGuideText;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface ConciergeBranchGuideTextRepository extends ReactiveCrudRepository<ConciergeBranchGuideText, Integer> {

    Flux<ConciergeBranchGuideText> findByConciergeBranchGuide(int conciergeBranchGuide);
}
