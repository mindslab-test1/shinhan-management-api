package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.ConciergeTooltip;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ConciergeTooltipRepository extends ReactiveCrudRepository<ConciergeTooltip, Integer> {
    Flux<ConciergeTooltip> findAll();
    Mono<ConciergeTooltip> findById(int id);
    Flux<ConciergeTooltip> findByIdGreaterThan(int id);
    Flux<ConciergeTooltip> findAllByOrderByIdAsc();
}
