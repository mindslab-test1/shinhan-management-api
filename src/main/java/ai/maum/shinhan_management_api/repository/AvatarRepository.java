package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.Avatar;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface AvatarRepository  extends ReactiveCrudRepository<Avatar, Integer> {
    Mono<Avatar> findById(Integer id);
    Mono<Avatar> findTopById(int id);
}
