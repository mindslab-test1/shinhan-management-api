package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.Outfit;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface OutfitRepository extends ReactiveCrudRepository<Outfit, Integer> {
    Mono<Outfit> findTopById(int id);
}
