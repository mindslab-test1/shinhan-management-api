package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.Gesture;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface GestureRepository extends ReactiveCrudRepository<Gesture, Integer> {
    Mono<Gesture> findTopById(int id);
}
