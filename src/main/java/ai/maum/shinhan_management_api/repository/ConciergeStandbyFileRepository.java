package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.ConciergeStandbyFile;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface ConciergeStandbyFileRepository extends ReactiveCrudRepository<ConciergeStandbyFile, Integer> {
    Mono<Boolean> existsByName(String name);
    Mono<ConciergeStandbyFile> findByName(String name);
}
