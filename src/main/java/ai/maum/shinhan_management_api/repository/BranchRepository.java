package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.Branch;
import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

public interface BranchRepository extends ReactiveCrudRepository<Branch, Integer> {
    Mono<Branch> findById(int id);
    Mono<Branch> findByBranchNo(String branchNo);


    @Modifying
    @Transactional
    @Query("truncate table branch")
    Mono<Void> deleteAllItem();
}
