package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.AiHuman;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Mono;

public interface AiHumanRepository extends R2dbcRepository<AiHuman,Integer> {
    Mono<AiHuman> findTopById(int id);
}
