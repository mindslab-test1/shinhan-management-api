package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.ConciergeNotice;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface ConciergeNoticeRepository extends ReactiveCrudRepository<ConciergeNotice, Integer> {
    Mono<Long> countByDeviceAndBranchYnAndUseYn(Integer device, String branchYn, String useYn);
    Flux<ConciergeNotice> findByDeviceAndBranchYnAndUseYnOrderByPositionAsc(Integer device, String branchYn, String useYn);
    Flux<ConciergeNotice> findByDeviceAndPositionGreaterThan(Integer device, int position);
    @Query("select * from concierge_notice where device in(:targetDeviceList) and use_yn = 'Y'")
    Flux<ConciergeNotice> findAfterDelete(List<Integer> targetDeviceList);
    @Query("select * from concierge_notice where (device in(:targetDeviceList) or branch_yn = 'N') and use_yn = 'Y'")
    Flux<ConciergeNotice> findWithFullAfterDelete(List<Integer> targetDeviceList);
    Flux<ConciergeNotice> findByOrderByCreatedDescIdDesc(PageRequest pageRequest);
}
