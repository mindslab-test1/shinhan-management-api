package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.api.polling.dto.NamePair;
import ai.maum.shinhan_management_api.api.video.dto.VideoGroupDto;
import ai.maum.shinhan_management_api.entity.VideoDistribution;
import ai.maum.shinhan_management_api.entity.VideoDistributionGroup;
import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface VideoDistributionRepository extends ReactiveCrudRepository<VideoDistribution,Integer> {

    Mono<VideoDistribution> findById(int id);

    Flux<VideoDistribution> findAllByGroupId(int groupId);

    Flux<VideoDistribution> findAllByGroupIdAndStatusLessThan(int groupId, int status);

    Flux<VideoDistribution> findAllById(int id);

    @Query(value =
            "UPDATE video_distribution SET status=4 WHERE group_id =:groupId AND status=3")
    Mono<Void> updateVideoByStatusAndGroupId (int groupId);
//
    @Query(value =
            "select v.name, d.id from video_distribution d\n" +
                    "join video v on v.id = d.video\n" +
                    "left join video_distribution_group on  d.group_id = video_distribution_group.id\n" +
                    "left join device de on video_distribution_group.device  = de.id\n" +
                    "where video_distribution_group.status = 4 and\n" +
                    "d.status = 4 and\n" +
                    "de.device_no= :deviceNo and de.branch_no= :branchNo")
    Flux<NamePair> listDistributeCandidateVideoNames(String deviceNo, String branchNo);

    Mono<VideoDistribution> findTopByVideoAndStatus(int video, int status);

    @Modifying
    @Query("update video_distribution set status = :status where id = :id")
    Mono<Integer> updateStatusById(int id, int status);

    @Query(value = "select count(*) from video_distribution where group_id = :groupId and status = :status")
    Mono<Integer> getDistributedCnt(int groupId, int status);

    @Query(value = "select * from video_distribution where group_id = :groupId and status=4")
    Flux<VideoDistribution> findByGroupIdDeploy(int groupId);

    @Modifying
    @Query("update video_distribution set status = 1 where id = :id and status = 0;")
    Mono<Integer> updateStatusProgressCreateById(int id);

    @Modifying
    @Query("update video_distribution set status = 2 where id = :id and status = 1;")
    public Mono<Integer> updateStatusCompleteCreateById(int videoId);

    @Query("select group_id from video_distribution where video = :videoId")
    public Mono<Integer> findGroupIdByVideoId(int videoId);

    @Query("select count(*) from video_distribution vd " +
            "left join video_distribution_group vdg on vd.group_id = vdg.id " +
            "left join device d on vdg.device = d.id " +
            "where d.device_type in " +
            "(select d2.device_type from video_distribution vd2 " +
            "left join video_distribution_group vdg2 on vd2.group_id = vdg2.id " +
            "left join device d2 on vdg2.device = d2.id " +
            "where vd2.video = :videoId) " +
            "and vd.status = 1;")
    public Mono<Integer> countCreateWorking(int videoId);

    @Modifying
    @Query("update video_distribution set status = 1 " +
            "where id in(:ids1)" +
            "and (select count(*) from video_distribution where id in(:ids2) and status = 0) = :reqCnt;")
    public Mono<Integer> updateStatusProgressCreateByIds(List<Integer> ids1, List<Integer> ids2, int reqCnt);

    @Query("select * from video_distribution vd " +
            "left join video_distribution_group vdg on vd.group_id = vdg.id " +
            "left join device d on vdg.device = d.id " +
            "where vd.video = :videoId and vd.status = 5 and d.branch_no = :branchNo and d.device_no = :deviceNo")
    public Mono<VideoDistribution> findDistributingByVideoAndBranchNoAndDeviceNo(int videoId, String branchNo, String deviceNo);

    @Modifying
    @Query("update video_distribution set status = 6 where id = :id and status = 5")
    public Mono<Integer> updateStatusCompleteRelease(int id);
}
