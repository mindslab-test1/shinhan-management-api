package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.Engine;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface EngineRepository extends ReactiveCrudRepository<Engine, Integer> {
    Mono<Engine> findTopById(int id);
}
