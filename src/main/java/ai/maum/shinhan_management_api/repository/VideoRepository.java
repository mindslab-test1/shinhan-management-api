package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.api.video.dto.CreateVideo;
import ai.maum.shinhan_management_api.entity.Video;
import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface VideoRepository extends ReactiveCrudRepository<Video,Integer> {

    Mono<Video> findById(int id);
    Mono<String> findNameById(int id);

    Flux<Video> findAllById(Flux<Integer> id);
    @Query("select vd.id, vd.group_id, v.id as video, v.avatar, v.outfit, v.background, v.resolution, v.scenario, v.gesture, v.engine from video v " +
            "left join video_distribution vd on v.id = vd.video " +
            "left join video_distribution_group vdg on vd.group_id = vdg.id " +
            "left join device d on vdg.device = d.id " +
            "where d.device_type=:deviceType and vd.status = 0 limit :targetSize;")
    public Flux<CreateVideo> findCreateTargets(int deviceType, int targetSize);

    @Query("select id from video where name = :name")
    public Mono<Integer> findIdByName(String name);

    @Modifying
    @Query("update video set name = :name where id = :id")
    public Mono<Integer> updateNameById(int id, String name);

    @Query("select vs.id, vs.group_id, v.id as video, v.avatar, v.outfit, v.background, v.resolution, v.scenario, v.gesture, v.engine from video v " +
            "left join video_synthesis vs on v.id = vs.video " +
            "left join video_synthesis_group vsg on vs.group_id = vsg.id " +
            "left join device d on vsg.device = d.id " +
            "where v.id = :id;")
    public Mono<CreateVideo> findCreateTarget(int id);

    public Mono<Video> findTopByEngineAndAvatarAndOutfitAndBackgroundAndResolutionAndGestureAndScenarioAndScenarioVersion(int engine, int avatar, int outfit, int background, int resolution, int gesture, int scenario, int scenarioVersion);

    public Mono<Video> findTopByScenarioAndScenarioVersionAndEngineAndAvatarAndOutfitAndBackgroundAndResolution(int scenario, int scenarioVersion, int engine, int avatar, int outfit, int background, int resolution);

    @Query("update video set size = :size where id = :id")
    public Mono<Integer> updateSizeById(int id, long size);

    public Mono<Video> findTopByName(String name);


    @Query("select * from video where name = :name")
    public Mono<Video> findByNameRegistSize(String name);

    @Query("update video set size= :size where name= :name")
    public Mono<Integer> updateVideoSize(long size, String name);

    @Query("select * from video v " +
            "left join scenario s on v.scenario = s.id " +
            "where v.engine = :engine and v.avatar = :avatar and v.outfit = :outfit and v.background = :background and v.resolution = :resolution and v.scenario_version = :scenarioVersion and s.device_type = :deviceType")
    public Flux<Video> findByEngineAndAvatarAndOutfitAndBackgroundAndResolutionAndScenarioVersionAndDeviceType(int engine, int avatar, int outfit, int background, int resolution, int scenarioVersion, int deviceType);
}
