package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.Background;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface BackgroundRepository extends ReactiveCrudRepository<Background, Integer> {
    Mono<Background> findTopById(int id);
}
