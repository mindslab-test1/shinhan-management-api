package ai.maum.shinhan_management_api.repository;

import ai.maum.shinhan_management_api.entity.ConciergeStandby;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface ConciergeStandbyRepository extends ReactiveCrudRepository<ConciergeStandby, Integer> {
    Flux<ConciergeStandby> findByConfigIdOrderByPosition(int configId);
    Flux<ConciergeStandby> findByConfigId(int configId);
}
