package ai.maum.shinhan_management_api.eai.test.controller;

import ai.maum.shinhan_management_api.eai.test.service.EaiService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/eai")
public class EaiController {
    private final EaiService eaiService;

    @PostMapping("/test/readEai")
    public void postEaiBranchData() {
        eaiService.readEai();
    }
}
