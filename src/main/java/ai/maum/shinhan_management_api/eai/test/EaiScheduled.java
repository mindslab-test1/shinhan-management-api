package ai.maum.shinhan_management_api.eai.test;

import ai.maum.shinhan_management_api.eai.test.service.EaiService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
@RequiredArgsConstructor
@Component
public class EaiScheduled {
    private final EaiService service;

    @Scheduled(cron = "${eai.scheduled}")
    public void eaiScheduled() throws IOException, InterruptedException {
        service.readEai();
    }
}
