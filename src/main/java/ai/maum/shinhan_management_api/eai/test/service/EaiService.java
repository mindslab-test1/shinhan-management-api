package ai.maum.shinhan_management_api.eai.test.service;

import ai.maum.shinhan_management_api.entity.Branch;
import ai.maum.shinhan_management_api.repository.BranchRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

@Service
@RequiredArgsConstructor
@Slf4j
public class EaiService {

    @Value("${eai.file.dir}")
    private String eaiFileDir;
    @Value("${eai.split.text}")
    private String eaiSplitText;


    private final BranchRepository branchEaiRepository;


    public Mono<Void> readEai() {
        log.info("EAI FILE DIR ::: "+ eaiFileDir);
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(eaiFileDir));
            FileWriter fileWriter = new FileWriter("readEAI.txt", true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            String readLine;
            String branchNoEai;
            String branchNameEai;
            String branchAddressEai;
            while ((readLine = bufferedReader.readLine()) != null) {
                String[] branchData = readLine.split(eaiSplitText);
                for (String branchDataLine : branchData){

                    branchNoEai = branchDataLine.split("\\|")[1];
                    branchNameEai = branchDataLine.split("\\|")[6];
                    branchAddressEai = branchDataLine.split("\\|")[20];

//                    branchEaiRepository.deleteAllItem().then(
//                            branchEaiRepository.save(
//                                    Branch.builder()
//                                            .branchName(branchNameEai)
//                                            .branchAddress(branchAddressEai)
//                                            .branchNo(branchNoEai).build())).subscribe();
                    bufferedWriter.write(branchNameEai + "|"+ branchNoEai + "|"+ branchAddressEai );
                    bufferedWriter.newLine();
                    log.debug("Create branch data ::: "+ branchNoEai +branchNameEai +branchAddressEai );
                }
                bufferedWriter.flush();
            }
            log.info("SUCCESS Update branch table");
        } catch (Exception e) {
            log.info("ERROR EAI branch update ");
        }
        return Mono.empty();
    }
}
