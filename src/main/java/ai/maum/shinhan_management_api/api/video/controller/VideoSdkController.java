package ai.maum.shinhan_management_api.api.video.controller;

import ai.maum.shinhan_management_api.api.video.dto.VideoFindDto;
import ai.maum.shinhan_management_api.api.video.dto.VideoFindResponseDto;
import ai.maum.shinhan_management_api.api.video.service.VideoFindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("api/v1/video/sdk/")
public class VideoSdkController {
    @Autowired
    private VideoFindService videoFindService;

    // INF-AHM-076
    @PostMapping("find")
    public Mono<VideoFindResponseDto> ahm076(@RequestHeader("deviceNo") String deviceNo,
                                             @RequestHeader("branchNo") String branchNo,
                                             @RequestBody VideoFindDto requestDto) {
        return videoFindService.ahm076(deviceNo, branchNo, requestDto);
    }
}
