package ai.maum.shinhan_management_api.api.video.dto;

import lombok.Builder;
import lombok.Data;

@Builder(toBuilder = true)
@Data
public class VideoFindResponseDto {
    public Video video;

    @Builder
    public static class Video {
        public String name;
        public String scenarioVersion;
        public String engineVersion;
        public String utter;
        public String model;
        public String outfit;
        public Background background;
        public String resolution;
        public String orientation;
        public Boolean isWait;
        public Boolean isBehavior;

        @Builder
        public static class Background {
            public String name;
        }
    }
}
