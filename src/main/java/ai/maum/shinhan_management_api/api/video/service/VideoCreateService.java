package ai.maum.shinhan_management_api.api.video.service;

import ai.maum.shinhan_management_api.api.human.dto.PageResponseDto;
import ai.maum.shinhan_management_api.api.video.dto.*;
import ai.maum.shinhan_management_api.common.Common;
import ai.maum.shinhan_management_api.entity.Engine;
import ai.maum.shinhan_management_api.entity.Resolution;
import ai.maum.shinhan_management_api.entity.Video;
import ai.maum.shinhan_management_api.repository.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class VideoCreateService {

    private final VideoRepository videoRepository;
    private final BranchRepository branchRepository;
    private final DeviceRepository deviceRepository;
    private final DeviceTypeRepository deviceTypeRepository;
    private final ScenarioRepository scenarioRepository;
    private final AvatarRepository avatarRepository;
    private final OutfitRepository outfitRepository;
    private final BackgroundRepository backgroundRepository;
    private final ResolutionRepository resolutionRepository;
    private final GestureRepository gestureRepository;
    private final EngineRepository engineRepository;
    private final WebClient cachingServerWebClient;

    private final VideoDistributionRepository videoDistributionRepository;
    private final VideoDistributionGroupRepo videoDistributionGroupRepo;
    private final CommonVideoService commonVideoService;

    @Value("${api.file.dir}")
    private String filePathDir;

    @Value("${read.filesize.dir}")
    private String fileSizeReadDir;

    @Value("${create.capacity}")
    private int createCapacity;

    public Flux<VideoGroupResponseDto> getCreatedGroupList(int deviceType) {
        return videoDistributionGroupRepo.findAllByStatusIsLike(Common.CREATED)
                .filterWhen(vg -> deviceRepository.findById(vg.getDevice())
                        .map(d -> {
                            if(d.getDeviceType() == deviceType) return true;
                            else return false;
                        }))
                .flatMap(vg -> deviceRepository.findById(vg.getDevice())
                        .flatMap(d -> branchRepository.findByBranchNo(d.getBranchNo())
                                .map(b -> VideoGroupDto.builder().device(vg.getDevice()).branch(b.getId()).groupId(vg.getId()).count(vg.getTotalCnt()).build())))
                .concatMap(commonVideoService::getReleaseGroupInfo);
    }

    public Flux<VideoResponseDto> getCVideoListWithGroup(int groupId, int status){
        return videoDistributionGroupRepo.findById(groupId)
                .flatMapMany(vg -> videoDistributionRepository.findAllByGroupIdAndStatusLessThan(vg.getId(), status))
                .concatMap( v -> commonVideoService.getReleaseVideoById(v.getId()));
    }

    public Mono<List<VideoGroupResponseDto>> getCreatedGroupListWithPage(int deviceType, int page) {
        log.info("기기 종류 " + deviceType + "의 " + page + "페이지 생성된 영상 그룹 목록 조회 요청");

        int startIndex = (page-1)*10;
        return getCreatedGroupList(deviceType)
                .collectList()
                .map(list -> {
                    if(list.size() <startIndex) return new LinkedList<VideoGroupResponseDto>();
                    if(list.size() < startIndex + 10) return list.subList(startIndex, list.size());
                    else return list.subList(startIndex, startIndex + 10);
                });
    }

    public Mono<List<VideoResponseDto>> getVideoListWithPage(int groupId, int page){
        log.info("그룹 " + groupId + "의 " + page + "페이지 생성된 영상 목록 조회 요청");

        int startIndex = (page-1)*10;
        return getCVideoListWithGroup(groupId, Common.WAITING_TO_DISTRIBUTION)
                .collectList()
                .map(list -> {
                    if(list.size() <startIndex) return new LinkedList<VideoResponseDto>();
                    if(list.size() < startIndex + 10) return list.subList(startIndex, list.size());
                    else return list.subList(startIndex, startIndex + 10);
                });
    }

    public Flux<VideoResponseDto> getCreateVideoWithoutPage(int deviceType){
        log.info("기기 종류 " + deviceType + "의 생성할 영상 목록 조회 요청");

        return videoDistributionGroupRepo.findAllByStatusLessThan(Common.CREATED)
                .filterWhen(vg -> deviceRepository.findById(vg.getDevice())
                        .map(d -> {
                            if(d.getDeviceType() == deviceType) return true;
                            else return false;
                        }))
                .concatMap(vg -> getCVideoListWithGroup(vg.getId(), Common.CREATED));
    }

    public Mono<PageResponseDto> getVideoTotalPage(int groupId){
        log.info("그룹 " + groupId + "의 생성된 영상 목록 페이지 개수 조회 요청");
        return getCVideoListWithGroup(groupId, Common.WAITING_TO_DISTRIBUTION).count().map(Common::getTotalPage);
    }

    public Mono<PageResponseDto> getCreatedGroupTotalPage(int deviceType){
        log.info("기기 종류 " + deviceType + "의 생성된 영상 그룹 목록 페이지 개수 조회 요청");
        return getCreatedGroupList(deviceType).count().map(Common::getTotalPage);
    }

    public Mono<String> createDoneWatch (String fileName) throws IOException {
        File videoFile = new File(filePathDir+fileName);
        byte[] encoded = Base64.getEncoder().encode(Files.readAllBytes(videoFile.toPath()));
        return Mono.just(new String(encoded));
    }

    public Mono<Void> requestCreateVideo(DeviceTypeDto req){
        log.info("Web으로부터 영상 생성 요청(Device Type: " + req.getDeviceType() + ")");

        // 해당 기기 종류에 대한 영상을 capacity만큼 가져오기
        videoRepository
                .findCreateTargets(req.getDeviceType(), createCapacity)
                .collectList()
                .publishOn(Schedulers.boundedElastic())
                .subscribe(targets -> {
                    List<CreateVideoRequestDto.Source> list = new ArrayList<>();

                    for(CreateVideo target: targets){
                        createRequestDto(target, list);
                        log.debug("생성한 DTO의 ID: " + target.getId());
                    }

                    requestToCaching(targets, new CreateVideoRequestDto(list));
                    log.info("캐싱 서버로 영상 생성 요청");
                });

        return Mono.empty().then();
    }

    private void requestToCaching(List<CreateVideo> targets, CreateVideoRequestDto createVideoRequestDto){
        cachingServerWebClient
                .post()
                .uri("/caching/v1/api/create/request")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(createVideoRequestDto), CreateVideoRequestDto.class)
                .retrieve()
                .bodyToMono(CreateVideoResponseDto.class)
                .subscribe(res -> {
                    List<CreateVideoResponseDto.Source> fileList = res.getFileList();
                    Set<Integer> groupIds = new HashSet<>();

                    log.debug("캐싱 서버로부터 응답 받은 파일 목록: " + fileList);

                    // status 1로 변경 및 파일명 등록
                    for(int i=0; i<fileList.size(); i++){
                        int idx = i;

                        videoDistributionRepository
                                .updateStatusProgressCreateById(targets.get(i).getId())
                                .flatMap(_updatedCnt1 ->
                                        videoRepository
                                                .updateNameById(targets.get(idx).getVideo(), fileList.get(idx).getName()))
                                .subscribe(_updatedCnt2 ->
                                        log.info("생성 진행 중(status 1)인 영상(ID: " + targets.get(idx).getId() + ", 파일명: " + fileList.get(idx)));

                        groupIds.add(targets.get(i).getGroupId());
                    }

                    // 영상 그룹 status 1로 변경
                    for(int groupId: groupIds)
                        videoDistributionGroupRepo
                                .updateStatusProgressCreateById(groupId)
                                .subscribe(_updatedCnt ->
                                        log.info("생성 진행 중인 영상 그룹(ID: " + groupId + ")"));
                });
    }

    private void createRequestDto(CreateVideo target, List<CreateVideoRequestDto.Source> list){
        String avatar = avatarRepository.findById(target.getAvatar()).block().getDisplayName();
        String outfit = outfitRepository.findById(target.getOutfit()).block().getDisplayName();
        String background = backgroundRepository.findById(target.getBackground()).block().getImageUrl();
        String utter = scenarioRepository.findById(target.getScenario()).block().getUtter();
        Resolution resolution = resolutionRepository.findById(target.getResolution()).block();
        String orientation = (resolution.getWidth() > resolution.getHeight())
                ? "LANDSCAPE" : ((resolution.getWidth() < resolution.getHeight())
                ? "PORTRAIT" : "NONE");
        String gesture = gestureRepository.findById(target.getGesture()).block().getDisplayName();
        Engine engine = engineRepository.findById(target.getEngine()).block();

        list.add(CreateVideoRequestDto.Source.builder()
                .avatar(avatar)
                .outfit(outfit)
                .background(CreateVideoRequestDto.Source.Background.builder()
                        .url(background)
                        .build())
                .utter(utter)
                .resolution(CreateVideoRequestDto.Source.Resolution.builder()
                        .name(resolution.getName())
                        .width(resolution.getWidth())
                        .height(resolution.getHeight())
                        .build())
                .orientation(orientation)
                .gesture(gesture)
                .engine(CreateVideoRequestDto.Source.Engine.builder()
                        .ttsHost(engine.getTtsHost())
                        .ttsPort(engine.getTtsPort())
                        .ttsPath(engine.getTtsPath())
                        .w2lHost(engine.getW2lHost())
                        .w2lPort(engine.getW2lPort())
                        .w2lPath(engine.getW2lPath())
                        .version(engine.getVersion())
                        .build())
                .build()
        );
    }

    private Mono<Video> fileSizeSave(String fileName, Long size) {
        return videoRepository.findTopByName(fileName)
                .flatMap(video -> {
                    log.info("Video Id : " + video.getId() + "\n Name : " + fileName + "\n size : " + size);
                    video.setSize(size);
                    return videoRepository.save(video);
                });
    }

    public void fileRead() throws IOException {
        try {
            File readFolder = new File(fileSizeReadDir);
            log.info("Read file dir  : " + fileSizeReadDir);
            for (File file : readFolder.listFiles()) {
                if (file.isFile()) {
                    String videoName = file.getName().replace(".mp4", "") + ".mp4";
                    fileSizeSave(videoName, file.length()).subscribe();
                }
            }
        } catch (Exception e) {
            log.error("File read Error :" + e.getMessage());
        }
//        Files.walk(Paths.get(fileSizeReadDir)).mapToLong( files -> files.toFile()).forEach(System.out::println);
//        Files.walk(Paths.get(fileSizeReadDir)).map( files -> files.toFile())
//                .filter( files -> files.isFile())
//                .map(f -> f.length()).forEach(System.out::println);
//                .filter(Files::isRegularFile).forEach(System.out::println);
    }
}
