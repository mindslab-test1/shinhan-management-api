package ai.maum.shinhan_management_api.api.video.dto;

import lombok.Builder;
import lombok.ToString;

@ToString
@Builder(toBuilder = true)
public class VideoGroupResponseDto {
    public final int id;
    public final String deviceNo;
    public final String branchName;
    public final String model;
    public final String aiHumanUuid;
    public final Integer totalCnt;
    public final String releaseDate;
}
