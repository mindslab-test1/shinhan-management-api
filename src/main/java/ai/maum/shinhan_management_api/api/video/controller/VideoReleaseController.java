package ai.maum.shinhan_management_api.api.video.controller;


import ai.maum.shinhan_management_api.api.human.dto.PageResponseDto;
import ai.maum.shinhan_management_api.api.video.dto.DeviceTypeDto;
import ai.maum.shinhan_management_api.api.video.dto.VideoGroupResponseDto;
import ai.maum.shinhan_management_api.api.video.dto.VideoResponseDto;
import ai.maum.shinhan_management_api.api.video.service.CommonVideoService;
import ai.maum.shinhan_management_api.api.video.service.VideoReleaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("api/v1/video/release/")
public class VideoReleaseController {

    @Autowired
    VideoReleaseService videoReleaseService;

    @Autowired
    CommonVideoService commonVideoService;

    //INF-AHM-108
    @GetMapping("done/{groupId}/{page}")
    public Mono<List<VideoResponseDto>> getReleasedVideoGroupPage(@PathVariable int groupId,
                                                                  @PathVariable int page){
        return videoReleaseService.getVideoListWithPage(groupId, page);
    }

    //INF-AHM-109
    @GetMapping("done/list/{page}")
    public Mono<List<VideoGroupResponseDto>> getVideoGroupByPage(@PathVariable int page,
                                                                 @RequestParam int deviceType){
        return videoReleaseService.getReleasedGroupListWithPage(deviceType, page);
    }

    //INF-AHM-110
    @GetMapping("done/list/page")
    public Mono<PageResponseDto> getReleaseDoneVideoGroupTotalPageNum(@RequestParam int deviceType) {
        return videoReleaseService.getReleasedGroupTotalPage(deviceType);
    }

    //INF-AHM-111
    @GetMapping("done/page")
    public Mono<PageResponseDto> getReleaseDonePageNum(@RequestParam int groupId) {
        return videoReleaseService.getReleasedVideoTotalPage(groupId);
    }

    //INF-AHM-112
    @GetMapping("/done/watch")
    public Mono<String> getReleaseDoneVideoWatch(@RequestParam String fileName) throws IOException {
        return videoReleaseService.releaseDoneVideoWatch(fileName);
    }

    //INF-AHM-118
    @GetMapping("{id}")
    public Mono<VideoResponseDto> getReleaseVideo(@PathVariable int id){
        return commonVideoService.getReleaseVideoById(id);
    }

    //INF-AHM-119
    @GetMapping("list")
    public Flux<VideoResponseDto> getReleaseVideoList(@RequestParam int deviceType){
        return videoReleaseService.getReleaseVideoWithoutPage(deviceType);
    }

    //INF-AHM-120
    @PostMapping("")
    public void postVideoListDeploy (@RequestBody DeviceTypeDto req){
        videoReleaseService.videoDeploy(req);
    }

    //생성에서 배포로 변경 요청
    @PostMapping("reqDistribution")
    public void requestDistribution(){
        videoReleaseService.requestDistribution();
    }


    @GetMapping("show/button")
    public Mono<Boolean> getShowValue () {
        return videoReleaseService.showValue();
    }
}
