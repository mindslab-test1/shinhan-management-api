package ai.maum.shinhan_management_api.api.video.dto;

import lombok.Builder;
import lombok.ToString;

@ToString
@Builder(toBuilder = true)
public class VideoResponseDto {
    public final int id;
    public final String branchNo;
    public final String branchName;
    public final String displayName;
    public final String aiHumanUuid;
    public final String utter;
    public final String intent;
    public final String fileName;
    public final Integer groupId;
    public final Integer status;
}