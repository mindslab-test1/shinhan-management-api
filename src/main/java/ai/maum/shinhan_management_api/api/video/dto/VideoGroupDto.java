package ai.maum.shinhan_management_api.api.video.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VideoGroupDto {
    Integer device;
    Integer branch;
    Integer groupId;
    Integer count;
}
