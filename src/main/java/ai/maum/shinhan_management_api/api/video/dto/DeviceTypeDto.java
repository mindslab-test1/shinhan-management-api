package ai.maum.shinhan_management_api.api.video.dto;

import lombok.Data;

@Data
public class DeviceTypeDto {
    private int deviceType;
}
