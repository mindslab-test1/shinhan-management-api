package ai.maum.shinhan_management_api.api.video.dto;

import lombok.Data;

import java.util.List;

@Data
public class CreateVideoResponseDto {
    private List<Source> fileList;

    @Data
    public static class Source{
        private String name;
    }
}
