package ai.maum.shinhan_management_api.api.video.service;


import ai.maum.shinhan_management_api.api.human.dto.PageResponseDto;
import ai.maum.shinhan_management_api.api.video.dto.DeviceTypeDto;
import ai.maum.shinhan_management_api.api.video.dto.VideoGroupDto;
import ai.maum.shinhan_management_api.api.video.dto.VideoGroupResponseDto;
import ai.maum.shinhan_management_api.api.video.dto.VideoResponseDto;
import ai.maum.shinhan_management_api.common.Common;
import ai.maum.shinhan_management_api.entity.*;
import ai.maum.shinhan_management_api.exception.DBException;
import ai.maum.shinhan_management_api.repository.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class VideoReleaseService {

    private final VideoDistributionRepository videoDistributionRepository;
    private final DeviceRepository deviceRepository;
    private final BranchRepository branchRepository;
    private final VideoDistributionGroupRepo videoDistributionGroupRepo;
    private final CommonVideoService commonVideoService;

    @Value("${api.file.dir}")
    private String filePathDir;

    public Flux<VideoGroupResponseDto> getReleasedGroupList(int deviceType) {
        return videoDistributionGroupRepo.findAllByStatusIsLike(Common.DISTRIBUTED)
                .filterWhen(vg -> deviceRepository.findById(vg.getDevice())
                        .map(d -> {
                            if(d.getDeviceType() == deviceType) return true;
                            else return false;
                        }))
                .flatMap(vg -> deviceRepository.findById(vg.getDevice())
                        .flatMap(d -> branchRepository.findByBranchNo(d.getBranchNo())
                                .map(b -> VideoGroupDto.builder().device(vg.getDevice()).branch(b.getId()).groupId(vg.getId()).count(vg.getTotalCnt()).build())))
                .flatMap(commonVideoService::getReleaseGroupInfo);
    }

    public Flux<VideoResponseDto> getRVideoListWithGroup(int groupId){
        return videoDistributionGroupRepo.findById(groupId)
                .flatMapMany(vg -> videoDistributionRepository.findAllByGroupId(vg.getId()))
                .concatMap( v -> commonVideoService.getReleaseVideoById(v.getId()));
    }

    public Mono<List<VideoGroupResponseDto>> getReleasedGroupListWithPage(int deviceType, int page) {
        log.info("기기 종류 " + deviceType + "의 " + page + "페이지 배포된 영상 그룹 목록 요청");

        int startIndex = (page-1)*10;
        return getReleasedGroupList(deviceType)
                .collectList()
                .map(list -> {
                    if(list.size() <startIndex) return new LinkedList<VideoGroupResponseDto>();
                    if(list.size() < startIndex + 10) return list.subList(startIndex, list.size());
                    else return list.subList(startIndex, startIndex + 10);
                });
    }

    public Mono<List<VideoResponseDto>> getVideoListWithPage(int groupId, int page){
        log.info("그룹 " + groupId + "의 " + page + "페이지 배포 영상 목록 요청");

        int startIndex = (page-1)*10;
        return getRVideoListWithGroup(groupId)
                .collectList()
                .map(list -> {
                    if(list.size() <startIndex) return new LinkedList<VideoResponseDto>();
                    if(list.size() < startIndex + 10) return list.subList(startIndex, list.size());
                    else return list.subList(startIndex, startIndex + 10);
                });
    }

    public Flux<VideoResponseDto> getReleaseVideoWithoutPage(int deviceType){
        log.info("기기 종류 " + deviceType + "의 배포할 영상 목록 조회 요청");

        return videoDistributionGroupRepo.findAllByStatusBetween(Common.WAITING_TO_DISTRIBUTION, Common.DISTRIBUTING)
                .filterWhen(vg -> deviceRepository.findById(vg.getDevice())
                        .map(d -> {
                            if(d.getDeviceType()==deviceType) return true;
                            else return false;
                        }))
                .concatMap(vg -> getRVideoListWithGroup(vg.getId()))
                .filter(v -> {
                    if(v.status == Common.DISTRIBUTED) return false;
                    else return true;
                });

    }

    public Mono<PageResponseDto> getReleasedVideoTotalPage(int groupId){
        log.info("그룹 " + groupId + "의 배포된 영상 목록 페이지 개수 조회 요청");
        return getRVideoListWithGroup(groupId).count().map(Common::getTotalPage);
    }

    public Mono<PageResponseDto> getReleasedGroupTotalPage(int deviceType){
        log.info("기기 종류 " + deviceType + "의 배포된 영상 그룹 목록 페이지 개수 조회 요청");
        return getReleasedGroupList(deviceType).count().map(Common::getTotalPage);
    }

    public Mono<String> releaseDoneVideoWatch(String fileName) throws IOException {
        log.info("배포된 영상 요청: " + fileName);

        File videoFile = new File(filePathDir+fileName);
        byte[] encoded = Base64.getEncoder().encode(Files.readAllBytes(videoFile.toPath()));
        return Mono.just(new String(encoded));
    }
    public void videoDeploy(DeviceTypeDto req) {
        log.info("기기 종류 " + req.getDeviceType() + "에 대해 배포 요청");

        try {
            videoDistributionGroupRepo.findAllByDeviceTypeAndStatus(req.getDeviceType(), Common.WAITING_TO_DISTRIBUTION)
                    .map(vdGroup -> {
                        log.info("INFO: VideoReleaseService.videoDeploy 배포 대기 중인 그룹(status 3): " + vdGroup.getId());
                        videoDistributionRepository.updateVideoByStatusAndGroupId(vdGroup.getId()).subscribe();
                        return vdGroup;
                    })
                    .flatMap(vdGroup -> videoDistributionGroupRepo.updateVideoByStatusAndGroupId(vdGroup.getId()))
                    .subscribe(vdGroup -> {
                        log.info("INFO: VideoReleaseService.videoDeploy [MESSAGE] After update status : {}", vdGroup);
                    });
        } catch (Exception e){
            throw new DBException(HttpStatus.INTERNAL_SERVER_ERROR, "Fail Video Deploy With AiHuman");
        }
    }
    public Mono<Boolean> showValue() {
        return videoDistributionGroupRepo.countStatusValue().flatMap(
                value -> value > 0? Mono.just(true): Mono.just(false)
        );
    }

    public void requestDistribution(){
        Flux<VideoDistributionGroup> videoDistributionGroupFlux = videoDistributionGroupRepo.findAllByStatusIsLike(2);
        videoDistributionGroupFlux.subscribe(sg -> {
            sg.setStatus(Common.WAITING_TO_DISTRIBUTION);
            Mono<VideoDistributionGroup> vdmono = videoDistributionGroupRepo.save(sg);

            vdmono.subscribe(
                    vd -> {
                        videoDistributionRepository.findAllByGroupId(sg.getId())
                                .subscribe(vs -> {
                                    vs.setStatus(Common.WAITING_TO_DISTRIBUTION);
                                    videoDistributionRepository.save(vs)
                                            .subscribe(videoDistribution
                                                    -> log.info("INFO: VideoReleaseService.requestDistribution" +
                                                            " [message] change video status, videoId: {}, videoStatus: {}, groupId: {}",
                                                    vs.getId(), vs.getStatus(), vs.getGroupId()));
                                });
                    }
            );
        });
    }
}
