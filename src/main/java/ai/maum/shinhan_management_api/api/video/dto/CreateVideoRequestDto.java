package ai.maum.shinhan_management_api.api.video.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class CreateVideoRequestDto {
    private List<Source> videos;

    @Data
    @Builder
    public static class Source{
        private String avatar;
        private String outfit;
        private Background background;
        private String utter;
        private Resolution resolution;
        private String orientation;
        private String gesture;
        private Engine engine;

        @Data
        @Builder
        public static class Background{
            private String url;
        }

        @Data
        @Builder
        public static class Resolution{
            private String name;
            private int width;
            private int height;
        }

        @Data
        @Builder
        public static class Engine{
            private String ttsHost;
            private int ttsPort;
            private String ttsPath;
            private String w2lHost;
            private int w2lPort;
            private String w2lPath;
            private String version;
        }
    }
}
