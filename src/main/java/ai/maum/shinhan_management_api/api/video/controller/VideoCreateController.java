package ai.maum.shinhan_management_api.api.video.controller;

import ai.maum.shinhan_management_api.api.human.dto.PageResponseDto;
import ai.maum.shinhan_management_api.api.video.dto.DeviceTypeDto;
import ai.maum.shinhan_management_api.api.video.dto.VideoGroupResponseDto;
import ai.maum.shinhan_management_api.api.video.dto.VideoResponseDto;
import ai.maum.shinhan_management_api.api.video.service.CommonVideoService;
import ai.maum.shinhan_management_api.api.video.service.VideoCreateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("api/v1/video/create/")
public class VideoCreateController {

    @Autowired
    VideoCreateService videoCreateService;

    @Autowired
    CommonVideoService commonVideoService;

    //INF-AHM-114
    @GetMapping("{id}")
    public Mono<VideoResponseDto> getCreateVideoById(@PathVariable int id){
        return commonVideoService.getReleaseVideoById(id);
    }

    //INF-AHM-115
    @GetMapping("list")
    public Flux<VideoResponseDto> getCreateVideoListByGroup(@RequestParam int deviceType) {
        return videoCreateService.getCreateVideoWithoutPage(deviceType);
    }

    //INF-AHM-117
    @PostMapping
    public Mono<Void> requestCreateVideo(@RequestBody DeviceTypeDto req){
        return videoCreateService.requestCreateVideo(req);
    }

    //INF-AHM-121
    @GetMapping("done/list/{page}")
    public Mono<List<VideoGroupResponseDto>> getCreatedVideoGroup(@PathVariable int page,
                                                                  @RequestParam int deviceType){
        return videoCreateService.getCreatedGroupListWithPage(deviceType, page);
    }

    //INF-AHM-122
    @GetMapping("done/{groupId}/{page}")
    public Mono<List<VideoResponseDto>> getVideoByGroupPage(@PathVariable int groupId,
                                                            @PathVariable int page){
        return videoCreateService.getVideoListWithPage(groupId, page);
    }

    //INF-AHM-123
    @GetMapping("done/list/page")
    public Mono<PageResponseDto> getCreatedGroupPage(@RequestParam int deviceType){
        return videoCreateService.getCreatedGroupTotalPage(deviceType);
    }

    //INF-AHM-124
    @GetMapping("done/page/{groupId}")
    public Mono<PageResponseDto> getCreatedVideoPage(@PathVariable int groupId){
        return videoCreateService.getVideoTotalPage(groupId);
    }

    //INF-AHM-112
    @GetMapping("/done/watch")
    public Mono<String> getCreteDoneVideoWatch(@RequestParam String fileName) throws IOException {
        return videoCreateService.createDoneWatch(fileName);
    }

    @PostMapping("/filesize")
    public void registFileSize() throws IOException {
        videoCreateService.fileRead();
    }
}
