package ai.maum.shinhan_management_api.api.video.dto;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class CreateVideo {
    private int id;
    private int groupId;
    private int video;
    private int engine;
    private int avatar;
    private int outfit;
    private int background;
    private int resolution;
    private int gesture;
    private int scenario;
}
