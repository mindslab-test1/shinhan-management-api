package ai.maum.shinhan_management_api.api.video.service;

import ai.maum.shinhan_management_api.api.video.dto.VideoFindDto;
import ai.maum.shinhan_management_api.api.video.dto.VideoFindResponseDto;
import ai.maum.shinhan_management_api.entity.*;
import ai.maum.shinhan_management_api.exception.DataNotFoundException;
import ai.maum.shinhan_management_api.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.swing.text.html.Option;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.regex.Pattern;

@Service
@Slf4j
public class VideoFindService {
    @Autowired
    DeviceRepository deviceRepository;

    @Autowired
    AiHumanRepository aiHumanRepository;

    @Autowired
    ScenarioRepository scenarioRepository;

    @Autowired
    VideoRepository videoRepository;

    @Autowired
    EngineRepository engineRepository;

    @Autowired
    AvatarRepository avatarRepository;

    @Autowired
    OutfitRepository outfitRepository;

    @Autowired
    BackgroundRepository backgroundRepository;

    @Autowired
    ResolutionRepository resolutionRepository;

    @Autowired
    GestureRepository gestureRepository;

    @Autowired
    ScenarioVersionRepository scenarioVersionRepository;

    public Mono<VideoFindResponseDto> ahm076(String deviceNo, String branchNo, VideoFindDto requestBody) {
        String utter = requestBody.getUtter();
        log.info("REQUEST: VideoFindService.ahm076 [HEADER] branchNo: " + branchNo + ", deviceNo: " + deviceNo + " [BODY] utter: " + utter);

        // 디바이스 찾아온다
        Mono<Device> deviceMono = deviceRepository.findTopByDeviceNoAndBranchNoOrderByCreatedAsc(deviceNo, branchNo);

        // 설정 찾아온다
        Mono<AiHuman> aiHumanMono = deviceMono.flatMap((device) -> aiHumanRepository.findTopById(device.getAiHuman()));

        // 설정에 맞는 시나리오 찾아온다
        Mono<Scenario> scenarioMono = aiHumanMono.flatMap(
                (aiHuman) -> deviceMono.flatMap(
                        (device) -> {
                            return scenarioRepository.findTopByScenarioVersionAndDeviceTypeAndUtterOrderByCreatedAsc(aiHuman.getScenarioVersion(), device.getDeviceType(), scenarioTextToUtter(requestBody.utter));
                        }
                )
        );

        // 시나리오, 대사에 맞는 영상 찾아온다
        Mono<Video> videoMono = scenarioMono.flatMap(
                (scenario) -> aiHumanMono.flatMap(
                        (aiHuman) -> {
                            log.info("INFO: VideoFindService.ahm076 [SCENARIO] scenarioID: {}, utter: {}", scenario.getId(), scenario.getUtter());
                            log.info("INFO: VideoFindService.ahm076 [AI_HUMAN] id: {}, uuid: {}, deviceID: {}", aiHuman.getId(), aiHuman.getUuid(), aiHuman.getDevice());

                            Mono<Video> videoResult = videoRepository.findTopByEngineAndAvatarAndOutfitAndBackgroundAndResolutionAndGestureAndScenarioAndScenarioVersion(
                                    aiHuman.getEngine(),
                                    aiHuman.getAvatar(),
                                    aiHuman.getOutfit(),
                                    aiHuman.getBackground(),
                                    aiHuman.getResolution(),
                                    scenario.getGesture(),
                                    scenario.getId(),
                                    aiHuman.getScenarioVersion()
                            );

                            return videoResult.map(video -> {
                                return video;
                            });
                        }
                )
        );

        return videoMono.flatMap(video -> {
            log.info(" INFO: VideoFindService.ahm076 [VIDEO] videoId: {}, videoName: {}", video.getId(), video.getName());

            Mono<Engine> engineMono = engineRepository.findTopById(video.getEngine());
            Mono<Avatar> avatarMono = avatarRepository.findTopById(video.getAvatar());
            Mono<Outfit> outfitMono = outfitRepository.findTopById(video.getOutfit());
            Mono<Background> backgroundMono = backgroundRepository.findTopById(video.getBackground());
            Mono<Resolution> resolutionMono = resolutionRepository.findTopById(video.getResolution());
            Mono<Gesture> gestureMono = gestureRepository.findTopById(video.getGesture());
            Mono<ScenarioVersion> scenarioVersionMono = scenarioMono.flatMap((scenario -> scenarioVersionRepository.findTopById(scenario.getScenarioVersion())));


            return Flux.merge(
                    //  videoMono.map(video -> VideoFindResponseDto.Video.builder().name(video.getName().replace(".mp4", "")).build()),
                    scenarioVersionMono.map(scenarioVersion -> VideoFindResponseDto.Video.builder().scenarioVersion(scenarioVersion.getVersion()).build()),
                    engineMono.map(engine -> VideoFindResponseDto.Video.builder().engineVersion(engine.getVersion()).build()),
                    scenarioMono.map(scenario -> VideoFindResponseDto.Video.builder().utter(scenario.getUtter()).build()),
                    avatarMono.map(avatar -> VideoFindResponseDto.Video.builder().model(avatar.getDisplayName()).build()),
                    outfitMono.map(outfit -> VideoFindResponseDto.Video.builder().outfit(outfit.getDisplayName()).build()),
                    backgroundMono.map(background -> VideoFindResponseDto.Video.builder().background(VideoFindResponseDto.Video.Background.builder().name(background.getDisplayName()).build()).build()),
                    resolutionMono.map(resolution -> VideoFindResponseDto.Video.builder()
                            .resolution(resolution.getWidth() + "x" + resolution.getHeight())
                            .orientation(resolution.getWidth() > resolution.getHeight() ? "landscape" : "portrait").build()
                    ),
                    gestureMono.map(gesture -> VideoFindResponseDto.Video.builder()
                            .isWait(utter.equals("{{wait}}"))
                            .isBehavior(gesture.getId() != 1 && !utter.equals("{{wait}}"))
                            .build())
            ).reduce((dto, dto2) ->
                    VideoFindResponseDto.Video.builder()
                            .name(video.getName().replace(".mp4", ""))
                            .scenarioVersion(Optional.ofNullable(dto2.scenarioVersion).orElseGet(() -> dto.scenarioVersion))
                            .engineVersion(Optional.ofNullable(dto2.engineVersion).orElseGet(() -> dto.engineVersion))
                            .utter(Optional.ofNullable(dto2.utter).orElseGet(() -> dto.utter))
                            .model(Optional.ofNullable(dto2.model).orElseGet(() -> dto.model))
                            .outfit(Optional.ofNullable(dto2.outfit).orElseGet(() -> dto.outfit))
                            .background(Optional.ofNullable(dto2.background).orElseGet(() -> dto.background))
                            .resolution(Optional.ofNullable(dto2.resolution).orElseGet(() -> dto.resolution))
                            .orientation(Optional.ofNullable(dto2.orientation).orElseGet(() -> dto.orientation))
                            .isWait(Optional.ofNullable(dto2.isWait).orElseGet(() -> dto.isWait))
                            .isBehavior(Optional.ofNullable(dto2.isBehavior).orElseGet(() -> dto.isBehavior))
                            .build()).map(
                    (videoResult) -> VideoFindResponseDto.builder().video(videoResult).build()
            );
        }).switchIfEmpty(Mono.error(new DataNotFoundException(HttpStatus.NOT_FOUND, "NOT FOUND: video data is empty")))
                .doOnError(err ->
                        log.error("ERROR: VideoFindService.ahm076 [MESSAGE] branchNo: " + branchNo + ", deviceNo: " + deviceNo + "Could not find video with utterance: " + requestBody.getUtter() + "")
                );
    }

    private String scenarioTextToUtter(String scenarioText) {
        return scenarioText.replaceAll("<.*?>", "").replaceAll("##", " ").replaceAll("%", " ").replaceAll("\n", " ").replaceAll(" +", " ");
    }
}
