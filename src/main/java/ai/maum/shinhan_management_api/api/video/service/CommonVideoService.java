package ai.maum.shinhan_management_api.api.video.service;

import ai.maum.shinhan_management_api.api.video.dto.VideoGroupDto;
import ai.maum.shinhan_management_api.api.video.dto.VideoGroupResponseDto;
import ai.maum.shinhan_management_api.api.video.dto.VideoResponseDto;
import ai.maum.shinhan_management_api.entity.*;
import ai.maum.shinhan_management_api.repository.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class CommonVideoService {

    private final VideoRepository videoRepository;
    private final VideoDistributionRepository videoDistributionRepository;
    private final BranchRepository branchRepository;
    private final DeviceRepository deviceRepository;
    private final DeviceTypeRepository deviceTypeRepository;
    private final ScenarioRepository scenarioRepository;
    private final VideoDistributionGroupRepo videoDistributionGroupRepo;

    public Mono<VideoResponseDto> getReleaseVideoById(int id){
        log.info("영상 조회 요청: (id: " + id + ")");

        return videoDistributionRepository.findById(id)
                .flatMap(videoDistribution -> {
                    Mono<Branch> branchMono = videoDistributionRepository.findById(id)
                            .flatMap(vd -> videoDistributionGroupRepo.findById(vd.getGroupId()))
                            .flatMap(vg -> deviceRepository.findById(vg.getDevice()))
                            .flatMap(d -> branchRepository.findByBranchNo(d.getBranchNo()));
                    Mono<Device> deviceMono = videoDistributionRepository.findById(id)
                            .flatMap(vd -> videoDistributionGroupRepo.findById(vd.getGroupId()))
                            .flatMap(vg -> deviceRepository.findById(vg.getDevice()));
                    Mono<DeviceType> deviceTypeMono = deviceMono.flatMap(d -> deviceTypeRepository.findById(d.getDeviceType()));
                    Mono<Scenario> scenarioMono = videoRepository.findById(videoDistribution.getVideo())
                            .flatMap(v -> scenarioRepository.findById(v.getScenario()));
                    Mono<Video> videoMono = videoRepository.findById(videoDistribution.getVideo());

                    return Flux.merge(
                            branchMono.map(b -> VideoResponseDto.builder().branchName(b.getBranchName()).branchNo(b.getBranchNo()).build()),
                            deviceMono.map(d -> VideoResponseDto.builder().aiHumanUuid(d.getAiHumanUuid()).build()),
                            scenarioMono.map(s -> VideoResponseDto.builder().utter(s.getUtter()).intent(s.getIntent()).build()),
                            deviceTypeMono.map(dt -> VideoResponseDto.builder().displayName(dt.getDisplayName()).build()),
                            videoMono.map(v -> VideoResponseDto.builder().fileName(v.getName()).build())
                    ).reduce((dto, dto2) -> dto.toBuilder()
                            .id(videoDistribution.getId())
                            .status(videoDistribution.getStatus())
                            .groupId(videoDistribution.getGroupId())
                            .branchName(Optional.ofNullable(dto2.branchName)
                                    .orElseGet(() -> dto.branchName))
                            .branchNo(Optional.ofNullable(dto2.branchNo)
                                    .orElseGet(() -> dto.branchNo))
                            .displayName(Optional.ofNullable(dto2.displayName)
                                    .orElseGet(() -> dto.displayName))
                            .aiHumanUuid(Optional.ofNullable(dto2.aiHumanUuid)
                                    .orElseGet(() -> dto.aiHumanUuid))
                            .utter(Optional.ofNullable(dto2.utter)
                                    .orElseGet(() -> dto.utter))
                            .intent(Optional.ofNullable(dto2.intent)
                                    .orElseGet(() -> dto.intent))
                            .fileName(Optional.ofNullable(dto2.fileName)
                                    .orElseGet(() -> dto.fileName))
                            .build());
                });
    }

    public Mono<VideoGroupResponseDto> getReleaseGroupInfo(VideoGroupDto videoGroupDto){
        Mono<Device> deviceMono = deviceRepository.findById(videoGroupDto.getDevice());
        Mono<Branch> branchMono = branchRepository.findById(videoGroupDto.getBranch());
        Mono<VideoDistributionGroup> videoDistributionGroupMono = videoDistributionGroupRepo.findById(videoGroupDto.getGroupId());

        return Flux.merge(
                deviceMono.map(d -> VideoGroupResponseDto.builder().deviceNo(d.getDeviceNo()).aiHumanUuid(d.getAiHumanUuid()).build()),
                branchMono.map(b -> VideoGroupResponseDto.builder().branchName(b.getBranchName()).build()),
                videoDistributionGroupMono.map(vdg -> VideoGroupResponseDto.builder().releaseDate(vdg.getCreated().toString()).build())
        ).reduce((dto, dto2) -> dto.toBuilder()
                .id(videoGroupDto.getGroupId())
                .totalCnt(videoGroupDto.getCount())
                .deviceNo(Optional.ofNullable(dto2.deviceNo)
                        .orElseGet(() -> dto.deviceNo))
                .aiHumanUuid(Optional.ofNullable(dto2.aiHumanUuid)
                        .orElseGet(() -> dto.aiHumanUuid))
                .branchName(Optional.ofNullable(dto2.branchName)
                        .orElseGet(() -> dto.branchName))
                .releaseDate(Optional.ofNullable(dto2.releaseDate)
                        .orElseGet(() -> dto.releaseDate))
                .build());
    }
}
