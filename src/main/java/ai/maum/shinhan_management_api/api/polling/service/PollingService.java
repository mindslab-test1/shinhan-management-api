package ai.maum.shinhan_management_api.api.polling.service;

import ai.maum.shinhan_management_api.api.polling.dto.AgentPollResponseDto;
import ai.maum.shinhan_management_api.entity.*;
import ai.maum.shinhan_management_api.exception.DataNotFoundException;
import ai.maum.shinhan_management_api.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class PollingService {
    @Autowired
    VideoDistributionGroupRepo videoDistributionGroupRepo;

    @Autowired
    DeviceRepository deviceRepository;

    @Autowired
    AiHumanRepository aiHumanRepository;

    @Autowired
    VideoDistributionRepository videoDistributionRepository;

    @Autowired
    VideoRepository videoRepository;

    @Autowired
    EngineRepository engineRepository;

    @Autowired
    AvatarRepository avatarRepository;

    @Autowired
    OutfitRepository outfitRepository;

    @Autowired
    BackgroundRepository backgroundRepository;

    @Autowired
    ResolutionRepository resolutionRepository;

    @Autowired
    GestureRepository gestureRepository;

    @Autowired
    ScenarioRepository scenarioRepository;
    @Autowired
    ScenarioVersionRepository scenarioVersionRepository;

    public Mono<AgentPollResponseDto> ahm071(String branchNo, String deviceNo) {
        if (branchNo == null) {
            log.error("지점번호가 존재하지 않음");
            throw new RuntimeException("/api/v1/poll/agent RuntimeException: branchNo is null");
        }
        if (deviceNo == null) {
            log.error("기기번호가 존재하지 않음");
            throw new RuntimeException("/api/v1/poll/agent RuntimeException: deviceNo is null");
        }

        log.info("지점번호 " + branchNo + ", 기기번호 " + deviceNo + "의 폴링 요청");

        Mono<Integer> videoDistributionGroupId = videoDistributionGroupRepo.findGroupIdByDeviceNoAndBranchNo(deviceNo, branchNo);
        // 배포할 영상이 있는지 본다 distribution status == 1
        //videoDistributionGroupRepo.findAllByDeviceAndStatusEquals()
        Mono<VideoDistributionGroup> videoDistributionGroupFlux = videoDistributionGroupRepo.findById(videoDistributionGroupId);
//        Flux<VideoDistribution> videoDistributionFlux = videoDistributionRepository.findByGroupIdDeploy(videoDistributionGroupId);
//            videoDistributionFlux.map(vd -> {
//                vd.setStatus(5);
//                videoDistributionRepository.save(vd).subscribe();
//                return vd;
//            });
        Mono<AgentPollResponseDto> agentPollResponseDtoMono = videoDistributionGroupFlux.flatMap(videoGroup -> {
                    log.info("폴링하는 기기의 영상 그룹: " + videoGroup);

                    Mono<AgentPollResponseDto.WaitVideo> waitVideoMono = getWaitVideo(branchNo, deviceNo);

                    Mono<AgentPollResponseDto.VersionUpdate> versionUpdateMono = deviceRepository.findTopByDeviceNoAndBranchNoOrderByCreatedAsc(deviceNo, branchNo)
                            .flatMap(device -> scenarioVersionRepository.findTopById(device.getScenarioVersion()))
                            .map(
                                    versionUpdate -> AgentPollResponseDto.VersionUpdate.builder()
                                            .scenarioVersion(versionUpdate.getVersion())
                                            .build()
                            );
                    List<AgentPollResponseDto.OldVideo> oldVideoFlux = new LinkedList<AgentPollResponseDto.OldVideo>();
                    Mono<List<AgentPollResponseDto.NewVideo>> newVideoFlux = videoDistributionRepository.findByGroupIdDeploy(videoGroup.getId()).flatMap(videoDistribution -> videoRepository.findById(videoDistribution.getVideo()))
                            .flatMap(video -> {
                                log.info("배포할 영상: " + video);

                                Mono<Engine> engineMono = engineRepository.findTopById(video.getEngine());
                                Mono<Avatar> avatarMono = avatarRepository.findTopById(video.getAvatar());
                                Mono<Outfit> outfitMono = outfitRepository.findTopById(video.getOutfit());
                                Mono<Background> backgroundMono = backgroundRepository.findTopById(video.getBackground());
                                Mono<Resolution> resolutionMono = resolutionRepository.findTopById(video.getResolution());
                                Mono<Scenario> scenarioMono = scenarioRepository.findById(video.getScenario());
                                Mono<ScenarioVersion> scenarioVersionMono = scenarioMono.flatMap((scenario -> scenarioVersionRepository.findTopById(scenario.getScenarioVersion())));

                                String videoName = video.getName();
                                int mp4Index = video.getName().indexOf(".mp4");
                                videoName = videoName.substring(0, mp4Index);

                                String responseVideoName = videoName;
                                return Flux.merge(
                                        engineMono.map(engine -> AgentPollResponseDto.NewVideo.builder().engineVersion(engine.getVersion()).build()),
                                        avatarMono.map(avatar -> AgentPollResponseDto.NewVideo.builder().model(avatar.getDisplayName()).build()),
                                        scenarioMono.map(scenario -> AgentPollResponseDto.NewVideo.builder().utter(scenario.getUtter()).build()),
                                        scenarioVersionMono.map(scenarioVersion -> AgentPollResponseDto.NewVideo.builder().scenarioVersion(scenarioVersion.getVersion()).build()),
                                        avatarMono.map(avatar -> AgentPollResponseDto.NewVideo.builder().model(avatar.getDisplayName()).build()),
                                        outfitMono.map(outfit -> AgentPollResponseDto.NewVideo.builder().outfit(outfit.getDisplayName()).build()),
                                        backgroundMono.map(background -> AgentPollResponseDto.NewVideo.builder().background(AgentPollResponseDto.NewVideo.Background.builder().name(background.getDisplayName()).build()).build()),
                                        resolutionMono.map(resolution -> AgentPollResponseDto.NewVideo.builder()
                                                .resolution(resolution.getWidth() + "x" + resolution.getHeight())
                                                .orientation(resolution.getWidth() > resolution.getHeight() ? "landscape" : "portrait").build())
                                ).reduce((dto, dto2) -> AgentPollResponseDto.NewVideo.builder()
                                        .name(responseVideoName)
                                        .scenarioVersion(Optional.ofNullable(dto2.scenarioVersion).orElseGet(() -> dto.scenarioVersion))
                                        .engineVersion(Optional.ofNullable(dto2.engineVersion).orElseGet(() -> dto.engineVersion))
                                        .utter(Optional.ofNullable(dto2.utter).orElseGet(() -> dto.utter))
                                        .model(Optional.ofNullable(dto2.model).orElseGet(() -> dto.model))
                                        .outfit(Optional.ofNullable(dto2.outfit).orElseGet(() -> dto.outfit))
                                        .background(Optional.ofNullable(dto2.background).orElseGet(() -> dto.background))
                                        .resolution(Optional.ofNullable(dto2.resolution).orElseGet(() -> dto.resolution))
                                        .orientation(Optional.ofNullable(dto2.orientation).orElseGet(() -> dto.orientation))
                                        .build()).switchIfEmpty(Mono.error(new DataNotFoundException(HttpStatus.NOT_FOUND, "video data is empty")));
                            }).collectList();

                    return Flux.merge(
                            waitVideoMono.map(waitVideo -> AgentPollResponseDto.builder().waitVideo(waitVideo).build()),
                            versionUpdateMono.map(versionUpdate -> AgentPollResponseDto.builder().versionUpdate(versionUpdate).build()),
                            newVideoFlux.map(newVideos -> AgentPollResponseDto.builder().newVideos(newVideos).build())
                    ).reduce((dto, dto2) -> AgentPollResponseDto.builder()
                            .newVideos(Optional.ofNullable(dto2.newVideos).orElseGet(() -> dto.newVideos))
                            .versionUpdate(Optional.ofNullable(dto2.versionUpdate).orElseGet(() -> dto.versionUpdate))
                            .newVideos(Optional.ofNullable(dto2.newVideos).orElseGet(() -> dto.newVideos))
                            .waitVideo(Optional.ofNullable(dto2.waitVideo).orElseGet(()-> dto.waitVideo))
                            .oldVideos(oldVideoFlux).build()
                    );

                }
        ).doOnNext(agentPollResponseDto -> {
            log.info("지점번호 " + branchNo + ", 기기번호 " + deviceNo + "의 배포할 영상 목록 생성 완료");

            videoDistributionGroupFlux.map(vdg -> {
                vdg.setStatus(5);
                videoDistributionGroupRepo.save(vdg).subscribe();

                videoDistributionRepository.findByGroupIdDeploy(vdg.getId()).map(
                        tmp -> {
                            tmp.setStatus(5);
                            videoDistributionRepository.save(tmp).subscribe();
                            return tmp;
                        }
                ).subscribe();

                return vdg;
            }).subscribe();
        });

        return agentPollResponseDtoMono.switchIfEmpty(Mono.defer(() -> {
            List<AgentPollResponseDto.NewVideo> newVideos = new LinkedList<>();
            List<AgentPollResponseDto.OldVideo> oldVideos = new LinkedList<>();
            Mono<AgentPollResponseDto.WaitVideo> waitVideoMono = getWaitVideo(branchNo, deviceNo);

            return waitVideoMono.flatMap(waitVideo -> {
                return Mono.just(AgentPollResponseDto.builder().newVideos(newVideos).oldVideos(oldVideos)
                        .waitVideo(waitVideo).build());
            });

//            return Mono.just(AgentPollResponseDto.builder().newVideos(newVideos).oldVideos(oldVideos)
//                    .waitVideo(AgentPollResponseDto.WaitVideo.builder().build()).build());
        }));
        // 배포 영상 목록 생성


        // 버전 업데이트 확인
        // 나중에 시나리오 업데이트 들어갈 때부터 제대로 될 듯

        // 영상 삭제는 나중에 넣자
        // 개별 삭제 기능이 있음

//        AgentPollResponseDto.AgentPollResponseDtoBuilder builder = AgentPollResponseDto.builder();
//        builder.message(message);
//
//        AgentPollResponseDto res =
//                .newVideos()
//                .oldVideos()
//                .versionUpdate()
//                .build()
//
//                return Mono.just(res);
    }

    public Mono<AgentPollResponseDto.WaitVideo> getWaitVideo(String branchNo, String deviceNo) {
        return deviceRepository.findTopByDeviceNoAndBranchNoOrderByCreatedAsc(deviceNo, branchNo)
                .flatMap(device -> {
                    return scenarioRepository.findTopByScenarioVersionAndDeviceTypeAndUtterOrderByCreatedAsc(device.getScenarioVersion(), device.getDeviceType(), "{{wait}}")
                            .flatMap(scenario -> {
                                return aiHumanRepository.findTopById(device.getAiHuman())
                                        .flatMap(aiHuman -> {
                                            return videoRepository.findTopByScenarioAndScenarioVersionAndEngineAndAvatarAndOutfitAndBackgroundAndResolution(scenario.getId(), aiHuman.getScenarioVersion(), aiHuman.getEngine(), aiHuman.getAvatar(), aiHuman.getOutfit(), aiHuman.getBackground(), aiHuman.getResolution())
                                                    .flatMap(video -> scenarioVersionRepository.findTopById(video.getScenarioVersion())
                                                            .map(scenarioVersion -> AgentPollResponseDto.WaitVideo.builder().name(video.getName().replace(".mp4", "").replace(".mkv", "")).scenarioVersion(scenarioVersion.getVersion()).build())
                                                    );
                                        });
                            });

                });
    }
}