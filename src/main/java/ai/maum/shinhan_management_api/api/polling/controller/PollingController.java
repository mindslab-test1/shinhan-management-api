package ai.maum.shinhan_management_api.api.polling.controller;

import ai.maum.shinhan_management_api.api.polling.dto.AgentPollResponseDto;
import ai.maum.shinhan_management_api.api.polling.service.PollingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/poll")
public class PollingController {
    @Autowired
    PollingService pollingService;

    @PostMapping("/agent")
    public Mono<AgentPollResponseDto> ahm071(@RequestHeader String branchNo, @RequestHeader String deviceNo) {
        return pollingService.ahm071(branchNo, deviceNo);
    }
}
