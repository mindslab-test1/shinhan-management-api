package ai.maum.shinhan_management_api.api.polling.dto;

import lombok.Data;

@Data
public class NamePair {
    public String name;
    public int id;
}
