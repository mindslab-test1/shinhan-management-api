package ai.maum.shinhan_management_api.api.polling.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder(toBuilder = true)
public class AgentPollResponseDto {
    public String message;
    public List<NewVideo> newVideos;
    public VersionUpdate versionUpdate;
    public List<OldVideo> oldVideos;
    public WaitVideo waitVideo;

    @Data
    @Builder(toBuilder = true)
    public static class NewVideo {
        public String name;
        public String scenarioVersion;
        public String engineVersion;
        public String utter;
        public String model;
        public String outfit;
        public Background background;
        public String resolution;
        public String orientation;

        @Data
        @Builder(toBuilder = true)
        public static class Background {
            public String name;
        }
    }

    @Data
    @Builder(toBuilder = true)
    public static class VersionUpdate {
        public String scenarioVersion;
    }

    @Data
    @Builder(toBuilder = true)
    public static class OldVideo {
        public String name;
        public String scenarioVersion;
    }

    @Data
    @Builder(toBuilder = true)
    public static class WaitVideo {
        public String name;
        public String scenarioVersion;
    }
}
