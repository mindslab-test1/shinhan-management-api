package ai.maum.shinhan_management_api.api.bubble.dto;

import lombok.Builder;
import lombok.Data;

@Builder(toBuilder = true)
@Data
public class GetBubbleBackgroundResponseDto {
    String name;
}
