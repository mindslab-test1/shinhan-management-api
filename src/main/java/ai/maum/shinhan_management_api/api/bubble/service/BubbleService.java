package ai.maum.shinhan_management_api.api.bubble.service;

import ai.maum.shinhan_management_api.api.bubble.dto.GetBubbleBackgroundResponseDto;
import ai.maum.shinhan_management_api.entity.*;
import ai.maum.shinhan_management_api.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class BubbleService {
    @Autowired
    VideoDistributionGroupRepo videoDistributionGroupRepo;

    @Autowired
    DeviceRepository deviceRepository;

    @Autowired
    DeviceTypeRepository deviceTypeRepository;

    @Autowired
    AiHumanRepository aiHumanRepository;

    @Autowired
    VideoDistributionRepository videoDistributionRepository;

    @Autowired
    VideoRepository videoRepository;

    @Autowired
    EngineRepository engineRepository;

    @Autowired
    AvatarRepository avatarRepository;

    @Autowired
    OutfitRepository outfitRepository;

    @Autowired
    BackgroundRepository backgroundRepository;

    @Autowired
    ResolutionRepository resolutionRepository;

    @Autowired
    GestureRepository gestureRepository;

    @Autowired
    ScenarioRepository scenarioRepository;
    @Autowired
    ScenarioVersionRepository scenarioVersionRepository;

    public Mono<GetBubbleBackgroundResponseDto> getBubbleBackground(@RequestHeader("deviceNo") String deviceNo,
                                                                    @RequestHeader("branchNo") String branchNo) {
        // 디바이스 찾아온다
        Mono<Device> deviceMono = deviceRepository.findTopByDeviceNoAndBranchNoOrderByCreatedAsc(deviceNo, branchNo);

        // 설정 찾아온다
        Mono<AiHuman> aiHumanMono = deviceMono.flatMap(device -> aiHumanRepository.findTopById(device.getAiHuman()));

        Mono<Avatar> avatarMono = aiHumanMono.flatMap(aiHuman -> avatarRepository.findTopById(aiHuman.getAvatar()));

        Mono<DeviceType> deviceTypeMono = deviceMono.flatMap(device -> deviceTypeRepository.findTopById(device.getDeviceType()));

        Mono<Boolean> isDigitalDesk = deviceTypeMono.map(deviceType -> {
            return deviceType.getDisplayName().equals("digital_desk");
        });

        Mono<Boolean> isMan = avatarMono.map(avatar -> {
            return avatar.getDisplayName().equals("man");
        });
        //TODO 하드코딩 된 사항들 수정 필요
        return isDigitalDesk.flatMap(
                (_isDigitalDesk) -> {
                    if (_isDigitalDesk) {
                        return isMan.flatMap(_isMan -> {
                            if (_isMan)
                                return Mono.just(GetBubbleBackgroundResponseDto.builder().name("digitaldesk_man").build());
                            return Mono.just(GetBubbleBackgroundResponseDto.builder().name("digitaldesk_woman").build());
                        });
                    }
                    return isMan.flatMap(_isMan -> {
                        if (_isMan)
                            return Mono.just(GetBubbleBackgroundResponseDto.builder().name("kiosk_man").build());
                        return Mono.just(GetBubbleBackgroundResponseDto.builder().name("kiosk_woman").build());
                    });
                }
        ).switchIfEmpty(Mono.just(GetBubbleBackgroundResponseDto.builder().name("digitaldesk_man").build()));
    }
}
