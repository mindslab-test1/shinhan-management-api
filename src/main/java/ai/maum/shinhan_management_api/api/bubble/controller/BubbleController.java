package ai.maum.shinhan_management_api.api.bubble.controller;

import ai.maum.shinhan_management_api.api.bubble.dto.GetBubbleBackgroundResponseDto;
import ai.maum.shinhan_management_api.api.bubble.service.BubbleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("api/v1/bubble")
public class BubbleController {
    @Autowired
    BubbleService bubbleService;

    @PostMapping("/background")
    public Mono<GetBubbleBackgroundResponseDto> getBubbleBackground(@RequestHeader("deviceNo") String deviceNo,
                                                                    @RequestHeader("branchNo") String branchNo) {
        return bubbleService.getBubbleBackground(deviceNo, branchNo);
    }
}
