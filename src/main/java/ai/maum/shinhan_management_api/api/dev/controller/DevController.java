package ai.maum.shinhan_management_api.api.dev.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ai.maum.shinhan_management_api.api.dev.service.DevService;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/dev")
@RequiredArgsConstructor
public class DevController {
    private final DevService devService;
    
    // 요청한 상태로 시작 ID와 종료 ID까지의 기기에 대한 영상 목록 생성
    @PostMapping("/video-distribution")
    public Mono<Void> generateVideoDistribution(int startDeviceId, int endDeviceId, int status){
        return devService.genenrateVideoDistribution(startDeviceId, endDeviceId, status);
    }
}
