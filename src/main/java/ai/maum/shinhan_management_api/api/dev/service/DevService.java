package ai.maum.shinhan_management_api.api.dev.service;

import java.util.List;

import org.springframework.stereotype.Service;

import ai.maum.shinhan_management_api.entity.AiHuman;
import ai.maum.shinhan_management_api.entity.Video;
import ai.maum.shinhan_management_api.entity.VideoDistribution;
import ai.maum.shinhan_management_api.entity.VideoDistributionGroup;
import ai.maum.shinhan_management_api.repository.AiHumanRepository;
import ai.maum.shinhan_management_api.repository.DeviceRepository;
import ai.maum.shinhan_management_api.repository.VideoDistributionGroupRepo;
import ai.maum.shinhan_management_api.repository.VideoDistributionRepository;
import ai.maum.shinhan_management_api.repository.VideoRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Service
@RequiredArgsConstructor
@Slf4j
public class DevService {
    private final VideoDistributionGroupRepo videoDistributionGroupRepo;
    private final VideoDistributionRepository videoDistributionRepository;
    private final DeviceRepository deviceRepository;
    private final AiHumanRepository aiHumanRepository;
    private final VideoRepository videoRepository;

    public Mono<Void> genenrateVideoDistribution(int startDeviceId, int endDeviceId, int status){
        deviceRepository.findByIdInRange(startDeviceId, endDeviceId)
                .publishOn(Schedulers.boundedElastic())
                .subscribe(device -> {
                    AiHuman aiHuman = aiHumanRepository
                            .findById(device.getAiHuman())
                            .block();

                    List<Video> videos = videoRepository
                            .findByEngineAndAvatarAndOutfitAndBackgroundAndResolutionAndScenarioVersionAndDeviceType(
                                    aiHuman.getEngine(),
                                    aiHuman.getAvatar(),
                                    aiHuman.getOutfit(),
                                    aiHuman.getBackground(),
                                    aiHuman.getResolution(),
                                    aiHuman.getScenarioVersion(),
                                    device.getDeviceType()
                            ).collectList()
                            .block();

                    int cnt = 0;

                    if(status==2 || status==6) cnt = videos.size();

                    VideoDistributionGroup group = videoDistributionGroupRepo.save(VideoDistributionGroup.builder()
                            .device(device.getId())
                            .cnt(cnt)
                            .totalCnt(videos.size())
                            .status(status)
                            .build())
                            .block();

                    for(Video video: videos){
                        videoDistributionRepository
                                .save(VideoDistribution
                                        .builder()
                                        .video(video.getId())
                                        .groupId(group.getId())
                                        .scenarioVersion(device.getScenarioVersion())
                                        .status(status)
                                        .build())
                                .block();
                    }
                });

        return Mono.empty();
    }
}
