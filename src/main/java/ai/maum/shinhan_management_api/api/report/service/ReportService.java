package ai.maum.shinhan_management_api.api.report.service;

import ai.maum.shinhan_management_api.api.report.dto.AgentDownloadResponseDto;
import ai.maum.shinhan_management_api.api.report.dto.ReportCreateFromCachingRequestDto;
import ai.maum.shinhan_management_api.api.report.dto.ReportReleaseFromAgentRequestDto;
import ai.maum.shinhan_management_api.api.video.dto.CreateVideo;
import ai.maum.shinhan_management_api.api.video.dto.CreateVideoRequestDto;
import ai.maum.shinhan_management_api.api.video.dto.CreateVideoResponseDto;
import ai.maum.shinhan_management_api.entity.Engine;
import ai.maum.shinhan_management_api.entity.Resolution;
import ai.maum.shinhan_management_api.repository.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public class ReportService {
    private final long FIND_WAITING_TIME = 500L;
    private final VideoRepository videoRepository;
    private final VideoDistributionRepository videoDistributionRepository;
    private final ScenarioRepository scenarioRepository;
    private final AvatarRepository avatarRepository;
    private final VideoDistributionGroupRepo videoDistributionGroupRepo;
    private final OutfitRepository outfitRepository;
    private final BackgroundRepository backgroundRepository;
    private final ResolutionRepository resolutionRepository;
    private final GestureRepository gestureRepository;
    private final EngineRepository engineRepository;
    private final WebClient cachingServerWebClient;

    @Value("${create.capacity}")
    private int createCapacity;

    public Mono<Void> getCreateReport(ReportCreateFromCachingRequestDto req){
        log.info("캐싱 서버로부터 생성 결과 보고: file=" + req.getReport().getFile()
                + ", size=" + req.getReport().getSize()
                + ", result=" + req.getReport().getResult());

        // 완료한 영상의 ID 찾기
        videoRepository
                .findIdByName(req.getReport().getFile())
                .subscribe(videoId -> {
                    log.debug("생성 완료한 영상의 ID(videoId): " + videoId);

                    // 현재 생성 중인 영상의 총 개수 구하기
                    videoDistributionRepository
                            .countCreateWorking(videoId)
                            .subscribe(statusOneCnt -> {
                               // 다음 요청 가능한 개수
                               int nextReqCnt = createCapacity - statusOneCnt + 1;

                               // 완료한 영상의 생성 정보 찾기
                               videoDistributionRepository
                                       .findTopByVideoAndStatus(videoId, 1)
                                       .subscribe(completeVideo -> {
                                           log.debug("생성 완료한 영상: " + completeVideo);

                                           // 완료한 영상의 그룹 갱신
                                           videoDistributionGroupRepo
                                                   .updateCreatedCntAndStatusById(completeVideo.getGroupId())
                                                   .subscribe();

                                           // 완료한 영상 status 2로 변경
                                           videoDistributionRepository
                                                   .updateStatusCompleteCreateById(completeVideo.getId())
                                                   .subscribe(_updatedCnt -> {
                                                       log.info("생성 완료(status 2): " + req.getReport().getFile());

                                                       // 완료한 영상 파일 크기 갱신
                                                       videoRepository.updateSizeById(videoId, req.getReport().getSize()).subscribe();

                                                       if(nextReqCnt > 0){
                                                           // 기기 종류 찾기
                                                           videoDistributionGroupRepo
                                                                   .findDeviceTypeById(completeVideo.getGroupId())
                                                                   .subscribe(deviceType -> {
                                                                               log.debug("생성 완료한 영상의 기기 종류: " + deviceType);

                                                                               // 다음 요청 가능한 개수만큼 영상 가져오기
                                                                               videoRepository
                                                                                       .findCreateTargets(deviceType, nextReqCnt)
                                                                                       .collectList()
                                                                                       .subscribe(targets -> {
                                                                                           List<Integer> ids = new ArrayList<>();

                                                                                           for(CreateVideo target: targets)
                                                                                               ids.add(target.getId());

                                                                                           // 요청할 영상 한번에 status 1로 갱신하기
                                                                                           // 요청할 영상 중 하나라도 status가 0이 아닐 경우 갱신 중단
                                                                                           if(ids.size() > 0){
                                                                                               videoDistributionRepository
                                                                                                       .updateStatusProgressCreateByIds(ids, ids, ids.size())
                                                                                                       .publishOn(Schedulers.boundedElastic())
                                                                                                       .subscribe(updatedCnt -> {
                                                                                                           if(updatedCnt > 0){  // 갱신 성공했을 경우 요청
                                                                                                               List<CreateVideoRequestDto.Source> list = new ArrayList<>();

                                                                                                               for(CreateVideo target: targets){
                                                                                                                   createRequestDto(target, list);
                                                                                                                   log.debug("생성한 DTO의 ID: " + target.getId());
                                                                                                               }

                                                                                                               requestToCaching(targets, new CreateVideoRequestDto(list));
                                                                                                               log.info("캐싱 서버로 영상 생성 요청(status 1)");
                                                                                                           } else{  // 갱신 실패했을 경우 대기 후 재요청
                                                                                                               try {
                                                                                                                   Thread.sleep(FIND_WAITING_TIME);
                                                                                                                   findAgain(videoId, deviceType);
                                                                                                               } catch (InterruptedException e) {
                                                                                                                   e.printStackTrace();
                                                                                                               }
                                                                                                           }
                                                                                                       });
                                                                                           }
                                                                                       });
                                                                           });
                                                       }
                                                   });
                                       });
                            });
                });

        return Mono.empty().then();
    }

    public Mono<AgentDownloadResponseDto> getReleaseReport(String branchNo,
                                                           String deviceNo,
                                                           ReportReleaseFromAgentRequestDto req){
        String videoName = req.getVideo().getName().replace(".mp4", "") + ".mp4";
        log.info("지점번호 " + branchNo + ", 기기번호 " + deviceNo + "의 배포 결과 보고: name=" + videoName + ", size=" + req.getVideo().getSize());

        // 파일명으로 영상 정보 찾기
        return videoRepository
                .findTopByName(videoName)
                .flatMap(video -> {
                    // 다운로드 받은 파일 크기가 다를 경우
                    if(video.getSize()!=req.getVideo().getSize())
                        return Mono.just(AgentDownloadResponseDto.builder()
                                .result(false)
                                .build());

                    // 다운로드 받은 파일 크기가 일치할 경우
                    // 조건에 맞는 영상 찾기
                    return videoDistributionRepository
                                    .findDistributingByVideoAndBranchNoAndDeviceNo(video.getId(), branchNo, deviceNo)
                                    .flatMap(vd -> {
                                        // 영상 status 6으로 변경
                                        videoDistributionRepository
                                                .updateStatusCompleteRelease(vd.getId())
                                                .subscribe(_updatedCnt ->
                                                        log.info("지점번호 " + branchNo + ", 기기번호 " + deviceNo + "의 영상 배포 완료(status 6): (id: " + vd.getId() + ")"));

                                        // 영상이 포함된 그룹의 cnt를 증가시키면서 status 관리
                                        videoDistributionGroupRepo
                                                .updateReleasedCntAndStatusById(vd.getGroupId())
                                                .subscribe(_updatedCnt ->
                                                        log.debug("지점번호 " + branchNo + ", 기기번호 " + deviceNo + "의 영상 그룹 Cnt 갱신: (groupId: " + vd.getGroupId() + ")"));

                                        return Mono.just(AgentDownloadResponseDto.builder()
                                                .result(true)
                                                .build());
                                    });
                });
    }

    private void requestToCaching(List<CreateVideo> targets, CreateVideoRequestDto createVideoRequestDto){
        cachingServerWebClient
                .post()
                .uri("/caching/v1/api/create/request")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(createVideoRequestDto), CreateVideoRequestDto.class)
                .retrieve()
                .bodyToMono(CreateVideoResponseDto.class)
                .subscribe(res -> {
                    List<CreateVideoResponseDto.Source> fileList = res.getFileList();
                    Set<Integer> groupIds = new HashSet<>();

                    log.debug("캐싱 서버로부터 응답 받은 파일 목록: " + fileList);

                    // status 1로 변경 및 파일명 등록
                    for(int i=0; i<fileList.size(); i++){
                        int idx = i;

                        videoDistributionRepository
                                .updateStatusProgressCreateById(targets.get(i).getId())
                                .flatMap(_updatedCnt1 ->
                                        videoRepository
                                                .updateNameById(targets.get(idx).getVideo(), fileList.get(idx).getName()))
                                .subscribe(_updatedCnt2 ->
                                        log.info("생성 진행 중(status 1)인 영상(ID: " + targets.get(idx).getId() + ", 파일명: " + fileList.get(idx)));

                        groupIds.add(targets.get(i).getGroupId());
                    }

                    // 영상 그룹 status 1로 변경
                    for(int groupId: groupIds)
                        videoDistributionGroupRepo
                                .updateStatusProgressCreateById(groupId)
                                .subscribe(_updatedCnt ->
                                        log.info("생성 진행 중인 영상 그룹(ID: " + groupId + ")"));
                });
    }

    private void findAgain(int videoId, int deviceType){
        log.warn("재검색");

        // 현재 생성 중인 영상의 총 개수 구하기
        videoDistributionRepository
                .countCreateWorking(videoId)
                .subscribe(statusOneCnt -> {
                    // 다음 요청 가능한 개수
                    int nextReqCnt = createCapacity - statusOneCnt;

                    if(nextReqCnt > 0){
                        // 다음 요청 가능한 개수만큼 영상 가져오기
                        videoRepository
                                .findCreateTargets(deviceType, nextReqCnt)
                                .collectList()
                                .subscribe(targets -> {
                                    List<Integer> ids = new ArrayList<>();

                                    for(CreateVideo target: targets)
                                        ids.add(target.getId());

                                    // 요청할 영상 한번에 status 1로 갱신하기
                                    // 요청할 영상 중 하나라도 status가 0이 아닐 경우 갱신 중단
                                    if(ids.size() > 0){
                                        videoDistributionRepository
                                                .updateStatusProgressCreateByIds(ids, ids, ids.size())
                                                .publishOn(Schedulers.boundedElastic())
                                                .subscribe(updatedCnt -> {
                                                    if(updatedCnt > 0){  // 갱신 성공했을 경우 요청
                                                        List<CreateVideoRequestDto.Source> list = new ArrayList<>();

                                                        for(CreateVideo target: targets){
                                                            createRequestDto(target, list);
                                                            log.info("생성한 DTO의 ID: " + target.getId());
                                                        }

                                                        requestToCaching(targets, new CreateVideoRequestDto(list));
                                                        log.info("캐싱 서버로 영상 생성 요청(status 1)");
                                                    } else{  // 갱신 실패했을 경우 대기 후 재요청
                                                        try {
                                                            Thread.sleep(FIND_WAITING_TIME);
                                                            findAgain(videoId, deviceType);
                                                        } catch (InterruptedException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                });
                                    }
                                });
                    }
                });
    }

    private void createRequestDto(CreateVideo target, List<CreateVideoRequestDto.Source> list){
        String avatar = avatarRepository.findById(target.getAvatar()).block().getDisplayName();
        String outfit = outfitRepository.findById(target.getOutfit()).block().getDisplayName();
        String background = backgroundRepository.findById(target.getBackground()).block().getImageUrl();
        String utter = scenarioRepository.findById(target.getScenario()).block().getUtter();
        Resolution resolution = resolutionRepository.findById(target.getResolution()).block();
        String orientation = (resolution.getWidth() > resolution.getHeight())
                ? "LANDSCAPE" : ((resolution.getWidth() < resolution.getHeight())
                ? "PORTRAIT" : "NONE");
        String gesture = gestureRepository.findById(target.getGesture()).block().getDisplayName();
        Engine engine = engineRepository.findById(target.getEngine()).block();

        list.add(CreateVideoRequestDto.Source.builder()
                .avatar(avatar)
                .outfit(outfit)
                .background(CreateVideoRequestDto.Source.Background.builder()
                        .url(background)
                        .build())
                .utter(utter)
                .resolution(CreateVideoRequestDto.Source.Resolution.builder()
                        .name(resolution.getName())
                        .width(resolution.getWidth())
                        .height(resolution.getHeight())
                        .build())
                .orientation(orientation)
                .gesture(gesture)
                .engine(CreateVideoRequestDto.Source.Engine.builder()
                        .ttsHost(engine.getTtsHost())
                        .ttsPort(engine.getTtsPort())
                        .ttsPath(engine.getTtsPath())
                        .w2lHost(engine.getW2lHost())
                        .w2lPort(engine.getW2lPort())
                        .w2lPath(engine.getW2lPath())
                        .version(engine.getVersion())
                        .build())
                .build()
        );
    }
}
