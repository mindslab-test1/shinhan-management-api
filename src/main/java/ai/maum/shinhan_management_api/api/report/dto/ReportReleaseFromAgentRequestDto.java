package ai.maum.shinhan_management_api.api.report.dto;

import lombok.Data;

@Data
public class ReportReleaseFromAgentRequestDto {
    private Source video;

    @Data
    public class Source{
        private String name;
        private long size;
    }
}
