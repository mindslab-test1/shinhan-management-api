package ai.maum.shinhan_management_api.api.report.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AgentDownloadResponseDto {
    private boolean result;
}
