package ai.maum.shinhan_management_api.api.report.controller;

import ai.maum.shinhan_management_api.api.report.dto.AgentDownloadResponseDto;
import ai.maum.shinhan_management_api.api.report.dto.ReportCreateFromCachingRequestDto;
import ai.maum.shinhan_management_api.api.report.dto.ReportReleaseFromAgentRequestDto;
import ai.maum.shinhan_management_api.api.report.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("api/v1/report/")
public class ReportController {
    @Autowired
    private ReportService reportService;

    // INF-AHM-004
    @PostMapping("create")
    public Mono<Void> getCreateReport(@RequestBody ReportCreateFromCachingRequestDto req){
        return reportService.getCreateReport(req);
    }

    // INF-AHM-073
    @PostMapping("agent/download")
    public Mono<AgentDownloadResponseDto> getReleaseReport(@RequestHeader String branchNo,
                                                           @RequestHeader String deviceNo,
                                                           @RequestBody ReportReleaseFromAgentRequestDto req){
        return reportService.getReleaseReport(branchNo, deviceNo, req);
    }
}
