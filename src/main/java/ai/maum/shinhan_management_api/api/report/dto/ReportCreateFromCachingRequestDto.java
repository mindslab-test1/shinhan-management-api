package ai.maum.shinhan_management_api.api.report.dto;

import lombok.Data;

@Data
public class ReportCreateFromCachingRequestDto {
    private Source report;

    @Data
    public class Source{
        private String file;
        private long size;
        private String result;
    }
}
