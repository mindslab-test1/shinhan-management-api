package ai.maum.shinhan_management_api.api.human.service;

import ai.maum.shinhan_management_api.api.human.dto.DeviceListResponseDto;
import ai.maum.shinhan_management_api.api.human.dto.DeviceRequestDto;
import ai.maum.shinhan_management_api.api.human.dto.DeviceResponseDto;
import ai.maum.shinhan_management_api.api.human.dto.PageResponseDto;
import ai.maum.shinhan_management_api.common.Common;
import ai.maum.shinhan_management_api.entity.*;
import ai.maum.shinhan_management_api.exception.DBException;
import ai.maum.shinhan_management_api.exception.DataNotFoundException;
import ai.maum.shinhan_management_api.repository.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.File;
import java.nio.file.Files;
import java.sql.Timestamp;
import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class HumanService {
    private final DeviceRepository deviceRepository;
    private final DeviceTypeRepository deviceTypeRepository;
    private final BranchRepository branchRepository;
    private final AiHumanRepository aiHumanRepository;
    private final AiHumanPreviewRepository aiHumanPreviewRepository;
    private final AvatarRepository avatarRepository;
    private final BackgroundRepository backgroundRepository;
    private final OutfitRepository outfitRepository;

    @Value("${url.caching_server}")
    private String cachingServerUrl;

    @Value("${api.file.dir}")
    private String filePathDir;

    public Mono<DeviceResponseDto> getDetailDeviceInfoById(int id){
        log.info("인공인간 조회 요청: (id: " + id + ")");

        Mono<Device> deviceMono = deviceRepository.findById(id);
        Mono<DeviceType> deviceTyMono = deviceMono.flatMap(d -> deviceTypeRepository.findById(d.getId()));
        Mono<Branch> branchMono = deviceMono.flatMap(d -> branchRepository.findByBranchNo(d.getBranchNo()));
        Mono<AiHuman> aiHumanMono = deviceMono.flatMap(d -> aiHumanRepository.findById(d.getAiHuman()));
        Mono<Outfit> outfitMono = aiHumanMono.flatMap(a -> outfitRepository.findById(a.getOutfit()));
        Mono<Avatar> avatarMono = aiHumanMono.flatMap(a -> avatarRepository.findById(a.getAvatar()));
        Mono<Background> backgroundMono = aiHumanMono.flatMap(a -> backgroundRepository.findById(a.getBackground()));

        return Flux.merge(
                branchMono.map(b -> DeviceResponseDto.builder().branchName(b.getBranchName()).build()),
                deviceMono.map(d -> DeviceResponseDto.builder().aiHumanUuid(d.getAiHumanUuid())
                        .deviceNo(d.getDeviceNo()).releaseDate(d.getCreated().toString()).build()),
                outfitMono.map(o -> DeviceResponseDto.builder().outfitUrl(o.getDisplayImage()).build()),
                avatarMono.map(a -> DeviceResponseDto.builder().avatarUrl(a.getDisplayImage()).build()),
                backgroundMono.map(bg -> DeviceResponseDto.builder().backgroundUrl(bg.getImageUrl()).build())
        ).reduce((dto, dto2) -> dto.toBuilder()
                .id(id)
                .branchName(Optional.ofNullable(dto2.branchName).orElseGet(() -> dto.branchName))
                .aiHumanUuid(Optional.ofNullable(dto2.aiHumanUuid).orElseGet(() -> dto.aiHumanUuid))
                .releaseDate(Optional.ofNullable(dto2.releaseDate).orElseGet(() -> dto.releaseDate))
                .outfitUrl(Optional.ofNullable(dto2.outfitUrl).orElseGet(() -> dto.outfitUrl))
                .avatarUrl(Optional.ofNullable(dto2.avatarUrl).orElseGet(() -> dto.avatarUrl))
                .backgroundUrl(Optional.ofNullable(dto2.backgroundUrl).orElseGet(() -> dto.backgroundUrl)).build());
    }

    public Mono<DeviceResponseDto> getDeviceInfoById(int id){
        Mono<Device> deviceMono = deviceRepository.findById(id);
        Mono<Branch> branchMono = deviceMono.flatMap(d -> branchRepository.findByBranchNo(d.getBranchNo()));

        return Flux.merge(
                deviceMono.map(d -> DeviceResponseDto
                        .builder()
                        .id(d.getId())
                        .deviceNo(d.getDeviceNo())
                        .aiHumanUuid(d.getAiHumanUuid())
                        .releaseDate(d.getCreated().toString())
                        .build()),
                branchMono.map(b -> DeviceResponseDto.builder().branchName(b.getBranchName()).build())
        ).reduce((dto, dto2) -> dto.toBuilder()
                .id(Optional.ofNullable(dto2.id).orElseGet(() -> dto.id))
                .deviceNo(Optional.ofNullable(dto2.deviceNo).orElseGet(() -> dto.deviceNo))
                .aiHumanUuid(Optional.ofNullable(dto2.aiHumanUuid).orElseGet(() -> dto.aiHumanUuid))
                .releaseDate(Optional.ofNullable(dto2.releaseDate).orElseGet(() -> dto.releaseDate))
                .branchName(Optional.ofNullable(dto2.branchName).orElseGet(() -> dto.branchName)).build());
    }

    public Mono<List<DeviceResponseDto>> getDeviceInfoListWithPage(int page,
                                                                   int deviceType,
                                                                   String paramKey,
                                                                   String paramValue,
                                                                   String releaseDateStart, String releaseDateEnd){
        if(releaseDateStart != null && releaseDateEnd != null)
            log.info("날짜로 인공인간 목록 조회 요청: (page: " + page + ", deviceType: " + deviceType + ", " + releaseDateStart + "~" + releaseDateEnd + ") 검색");
        else{
            if(paramKey == null)
                log.info("인공인간 목록 조회 요청: (page: " + page + ", deviceType: " + deviceType + ") 전체 검색");
            else
                log.info("인공인간 목록 조회 요청: (page: " + page + ", deviceType: " + deviceType + ", key: " + paramKey + ", value: " + paramValue + ") 검색");
        }

        int startIndex = (page-1)*10;
        return getDeviceInfoList(deviceType, paramKey, paramValue, releaseDateStart, releaseDateEnd)
                .collectList()
                .map(list -> {
                    if(list.size() <startIndex) return new LinkedList<DeviceResponseDto>();
                    if(list.size() < startIndex + 10) return list.subList(startIndex, list.size());
                    else return list.subList(startIndex, startIndex + 10);
                });
    }

    public Mono<PageResponseDto> getDeviceInfoListTotalPage(int deviceType,
                                                            String paramKey,
                                                            String paramValue,
                                                            String releaseDateStart, String releaseDateEnd){
        if(releaseDateStart != null && releaseDateEnd != null)
            log.info("날짜로 인공인간 목록 페이지 개수 조회 요청: (deviceType: " + deviceType + ", " + releaseDateStart + "~" + releaseDateEnd + ")");
        else{
            if(paramKey == null)
                log.info("인공인간 목록 페이지 개수 조회 요청: (deviceType: " + deviceType  + ") 전체 검색");
            else
                log.info("인공인간 목록 페이지 개수 조회 요청: (deviceType: " + deviceType + ", key: " + paramKey + ", value: " + paramValue + ") 검색");
        }

        return getDeviceInfoList(deviceType, paramKey, paramValue, releaseDateStart, releaseDateEnd)
                .count().map(Common::getTotalPage);
    }

    public Flux<DeviceResponseDto> getDeviceInfoList(int deviceType,
                                                     String paramKey,
                                                     String paramValue,
                                                     String releaseDateStart, String releaseDateEnd){
        Flux<DeviceResponseDto> deviceList = deviceRepository.findByDeviceType(deviceType)
                .flatMap(d -> getDeviceInfoById(d.getId()));

        if(paramKey == null ){

        }else if(paramKey.equals("branchName")){
            deviceList = filterBranchName(deviceList, paramValue);
        }else if(paramKey.equals("deviceNo")){
            deviceList = filterDeviceNo(deviceList, paramValue);
        }else if(paramKey.equals("aiHumanUuid")){
            deviceList = filterAiHumanUuid(deviceList, paramValue);
        }

        if(releaseDateStart != null && releaseDateEnd != null){
            deviceList = filterDate(deviceList, releaseDateStart, releaseDateEnd);
        }

        return deviceList;
    }

    public Flux<DeviceResponseDto> filterBranchName(Flux<DeviceResponseDto> deviceList, String paramValue){
        return deviceList.filter(dl -> (dl.branchName.equals(paramValue)));
    }

    public Flux<DeviceResponseDto> filterDeviceNo(Flux<DeviceResponseDto> deviceList, String paramValue){
        return deviceList.filter(dl -> (dl.deviceNo.equals(paramValue)));
    }

    public Flux<DeviceResponseDto> filterAiHumanUuid(Flux<DeviceResponseDto> deviceList, String paramValue){
        return deviceList.filter(dl -> (dl.aiHumanUuid.equals(paramValue)));
    }

    public Flux<DeviceResponseDto> filterDate(Flux<DeviceResponseDto> deviceList, String startDate, String endDate){
        return deviceList.filter(dl -> {
            Timestamp start = Timestamp.valueOf(startDate.trim() + " 00:00:00");
            Timestamp end = Timestamp.valueOf(endDate.trim() + " 23:59:59");
            Timestamp release = Timestamp.valueOf(dl.releaseDate.replace('T', ' '));
            if(start.before(release) && release.before(end)) return true;
            else return false;
        });
    }

    @Transactional
    public Mono<Device> deviceRegistrationWithAiHuman(DeviceRequestDto deviceRequestDto){
        log.info("인공인간 등록 요청: " + deviceRequestDto);

        try {

            return deviceRepository
                    .findTopByBranchNoAndDeviceNoOrderByCreatedAsc(deviceRequestDto.getBranchNo(), deviceRequestDto.getDeviceNo())
                    .flatMap(device -> createAiHuman(device, deviceRequestDto)
                                .flatMap(aiHuman -> {
                                    device.setScenarioVersion(aiHuman.getScenarioVersion());
                                    device.setAiHuman(aiHuman.getId());
                                    device.setAiHumanUuid(getAiHumanUuid(device, aiHuman.getUuid()));
                                    device.setRegisterStatus(1);
                                    return deviceRepository.save(device);
                                }
                    ));
        } catch (Exception e){
            throw new DBException(HttpStatus.INTERNAL_SERVER_ERROR, "Device registration failed");
        }
    }

    @Transactional
    public Mono<Device> modifyDeviceWithAiHuman(DeviceRequestDto deviceRequestDto, int deviceId){
        log.info("인공인간(" + deviceId + ") 수정 요청: " + deviceRequestDto);

        try {
            deviceRequestDto.setDeviceId(deviceId);
            return registAiHuman(deviceRequestDto).flatMap( aiHumanData ->
                    deviceRepository.save(Device.builder()
                            .aiHuman(aiHumanData.getId())
                            .aiHumanUuid(aiHumanData.getUuid()).build()));
        } catch (Exception e){
            throw new DBException(HttpStatus.INTERNAL_SERVER_ERROR, "Fail modify Device With AiHuman");
        }
    }
    @Transactional
    public Mono<AiHuman> registAiHuman(DeviceRequestDto deviceRequestDto) {
        try {
            String aiHumanUuid = "82"
                    +"-"+deviceRequestDto.getDeviceNo()
                    +"-"+deviceRequestDto.getBranchNo()
                    +"-"+UUID.randomUUID().toString().substring(0,12);
            return aiHumanRepository.save(AiHuman.builder()
            .uuid(aiHumanUuid)
            .avatar(deviceRequestDto.getAvatar())
            .background(deviceRequestDto.getBackground())
            .outfit(deviceRequestDto.getOutfit())
            .device(deviceRequestDto.getDeviceId())
                    //TODO 추후 어떻게 지정될지 확인 및 ID 값 인터페이스 정의서에서 변경
            .engine(1)
            .resolution(1)
            .scenarioVersion(1)
            .build());
        }catch (Exception e){
            throw new DBException(HttpStatus.INTERNAL_SERVER_ERROR, "Fail regist AI human");
        }
    }
    public Mono<String> previewAiHuman(int avatar, int outfit, int background, int preview){
//        WebClient client = WebClient.builder()
//                .baseUrl(cachingServerUrl)
//                .defaultCookie("key","value")
//                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).build();
//
//        String response = client.get().uri(builder -> builder.path("/preview/aiHuman")
//                .queryParam("avatar",avatar)
//                .queryParam( "outfit",outfit)
//                .queryParam("background",background).build()).retrieve().bodyToMono(String.class).toString();
        log.info("인공인간 영상 미리보기 요청: (avatar: " + avatar + ", outfit: " + outfit + ", background: " + background + ", previewNum: " + preview + ")");

        try {

            Mono<String> fileName = aiHumanPreviewRepository.findPreviewFileName(avatar, outfit, background, preview)
                    .map(AiHumanPreview::getFileName);
            File previewAiHumanFile;
            previewAiHumanFile = new File(filePathDir+fileName);
            byte[] encoded = Base64.getEncoder().encode(Files.readAllBytes(previewAiHumanFile.toPath()));
            return Mono.just(new String(encoded));
        }catch (Exception e) {
            throw new DBException(HttpStatus.INTERNAL_SERVER_ERROR, "Fail search preview");
        }
    }

    @Transactional
    public Mono<AiHuman> createAiHuman(Device device, DeviceRequestDto deviceRequestDto){
        UUID uuid = UUID.randomUUID();
        Mono<Avatar> avatarMono = avatarRepository.findById(deviceRequestDto.getAvatar());
        Mono<Outfit> outfitMono = outfitRepository.findById(deviceRequestDto.getOutfit());
        Mono<Background> backgroundMono = backgroundRepository.findById(deviceRequestDto.getBackground());

        avatarMono.switchIfEmpty(Mono.error(new DataNotFoundException(HttpStatus.INTERNAL_SERVER_ERROR, "avatar is empty"))).subscribe();
        outfitMono.switchIfEmpty(Mono.error(new DataNotFoundException(HttpStatus.INTERNAL_SERVER_ERROR, "outfit is empty"))).subscribe();
        backgroundMono.switchIfEmpty(Mono.error(new DataNotFoundException(HttpStatus.INTERNAL_SERVER_ERROR, "background is empty"))).subscribe();

        return aiHumanRepository.save(
                AiHuman.builder()
                .uuid(uuid.toString())
                .device(device.getId())
                .engine(1)
                .avatar(deviceRequestDto.getAvatar())
                .outfit(deviceRequestDto.getOutfit())
                .background(deviceRequestDto.getBackground())
                .resolution(1)
                .scenarioVersion(1).build()
        );
    }

    public String getAiHumanUuid(Device device, String uuid){
        return "82"
                +"-"+device.getDeviceNo()
                +"-"+device.getBranchNo()
                +"-"+uuid;
    }

    public Mono<DeviceListResponseDto> getDeviceListForOne(String branchNo, int deviceType){
        log.info("지점에 등록된 기기 목록 요청(지점번호: " + branchNo + ", 기기종류: " + deviceType + ")");
        return deviceRepository.findByBranchNoAndDeviceType(branchNo, deviceType)
                .map(device -> DeviceListResponseDto.Source.builder()
                        .deviceNo(device.getDeviceNo())
                        .branchNo(device.getBranchNo())
                        .registerStatus(device.getRegisterStatus())
                        .build())
                .collectList()
                .map(deviceList -> DeviceListResponseDto.builder()
                        .deviceList(deviceList)
                        .build());
    }

    public Mono<DeviceListResponseDto> getDeviceListForAll(int page, int deviceType){
        log.info("모든 지점에 등록된 기기 목록 요청(기기종류: " + deviceType + ")");
        return deviceRepository.findByDeviceTypeLike(deviceType, PageRequest.of(page-1, Common.AIHUMAN_BRANCH_PAGE))
                .map(device -> DeviceListResponseDto.Source.builder()
                        .deviceNo(device.getDeviceNo())
                        .branchNo(device.getBranchNo())
                        .registerStatus(device.getRegisterStatus())
                        .build())
                .collectList()
                .map(deviceList -> DeviceListResponseDto.builder()
                        .deviceList(deviceList)
                        .build());
    }
}
