package ai.maum.shinhan_management_api.api.human.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class DeviceListResponseDto {
    private List<Source> deviceList;

    @Data
    @Builder
    public static class Source {
        private String deviceNo;
        private String branchNo;
        private int registerStatus;
    }
}
