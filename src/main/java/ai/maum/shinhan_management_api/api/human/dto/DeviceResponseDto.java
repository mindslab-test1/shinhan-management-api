package ai.maum.shinhan_management_api.api.human.dto;

import lombok.Builder;
import lombok.ToString;

@ToString
@Builder(toBuilder = true)
public class DeviceResponseDto {
    public final Integer id;
    public final String deviceNo;
    public final String branchName;
    public final String aiHumanUuid;
    public final String deviceVersion;
    public final String avatarUrl;
    public final String outfitUrl;
    public final String backgroundUrl;
    public final String releaseDate;
}
