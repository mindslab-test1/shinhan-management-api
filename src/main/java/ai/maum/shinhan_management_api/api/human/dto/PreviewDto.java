package ai.maum.shinhan_management_api.api.human.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PreviewDto {
    int id;
    int avatar;
    int background;
    String fileName;
}
