package ai.maum.shinhan_management_api.api.human.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
@Builder
public class DeviceSearchDto {

    @NotNull
    String device_no;
    @NotNull
    String device_type;
    @NotNull
    String branch_name;
    @NotNull
    String model;
    @NotNull
    String outfit;
    @NotNull
    String background;



}
