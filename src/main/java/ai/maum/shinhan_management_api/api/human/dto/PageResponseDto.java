package ai.maum.shinhan_management_api.api.human.dto;

import lombok.Data;

@Data
public class PageResponseDto {
    private int pageCnt;
}
