package ai.maum.shinhan_management_api.api.human.dto;

import lombok.Data;

@Data
public class DeviceRequestDto {
    private int deviceType;
    private int deviceId;
    private String deviceNo;
    private String branchNo;
    private String branchName;
    private int avatar;
    private int outfit;
    private int background;
}
