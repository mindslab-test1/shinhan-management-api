package ai.maum.shinhan_management_api.api.human.controller;

import ai.maum.shinhan_management_api.api.human.dto.DeviceListResponseDto;
import ai.maum.shinhan_management_api.api.human.dto.DeviceRequestDto;
import ai.maum.shinhan_management_api.api.human.dto.DeviceResponseDto;
import ai.maum.shinhan_management_api.api.human.dto.PageResponseDto;
import ai.maum.shinhan_management_api.api.human.service.HumanService;
import ai.maum.shinhan_management_api.entity.Device;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.annotation.Nullable;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/human")
public class HumanController {
    private final HumanService humanService;

    // INF-AHM-101
    @GetMapping("{id}")
    public Mono<DeviceResponseDto> getDeviceById(@PathVariable int id) {
        return humanService.getDetailDeviceInfoById(id);
    }

    // INF-AHM-102
    @GetMapping("list/{page}")
    public Mono<List<DeviceResponseDto>> getDeviceInfoByPage(@PathVariable int page,
                                                             @RequestParam int deviceType,
                                                             @RequestParam @Nullable String paramKey,
                                                             @RequestParam @Nullable String paramValue,
                                                             @RequestParam @Nullable String releaseDateStart,
                                                             @RequestParam @Nullable String releaseDateEnd){
        return humanService.getDeviceInfoListWithPage(page, deviceType, paramKey,
                paramValue, releaseDateStart, releaseDateEnd);
    }

    // INF-AHM-103
    @GetMapping("list/page")
    public Mono<PageResponseDto> getTotalPage(@RequestParam int deviceType,
                                              @RequestParam @Nullable String paramKey,
                                              @RequestParam @Nullable String paramValue,
                                              @RequestParam @Nullable String releaseDateStart,
                                              @RequestParam @Nullable String releaseDateEnd){
        return humanService.getDeviceInfoListTotalPage(deviceType, paramKey, paramValue, releaseDateStart, releaseDateEnd);
    }

    // INF-AHM-104
    @PostMapping
    public Mono<Device> postDeviceRegistrationAiHuman(@RequestBody DeviceRequestDto requestDto) {
        return humanService.deviceRegistrationWithAiHuman(requestDto);
    }

    // INF-AHM-105
    @GetMapping("list/device")
    public Mono<DeviceListResponseDto> getDeviceListForOne(@RequestParam String branchNo,
                                                           @RequestParam int deviceType) {
        return humanService.getDeviceListForOne(branchNo, deviceType);
    }

    // INF-AHM-106
    @GetMapping("list/device/all/{page}")
    public Mono<DeviceListResponseDto> getDeviceListForAll(@PathVariable int page,
                                                           @RequestParam int deviceType){
        return humanService.getDeviceListForAll(page, deviceType);
    }

    //INF-AHM-107
    @GetMapping("preview/{preview}")
    public Mono<String> previewAiHuman(@RequestParam int avatar, @RequestParam int outfit, @RequestParam int background, @PathVariable int preview){
        return humanService.previewAiHuman(avatar, outfit, background, preview);
    }
}
