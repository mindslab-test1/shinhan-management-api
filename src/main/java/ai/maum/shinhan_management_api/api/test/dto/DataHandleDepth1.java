package ai.maum.shinhan_management_api.api.test.dto;

import lombok.Data;

import java.util.List;


@Data
public class DataHandleDepth1 {

    public DataHandleDepth2 dataHandleDepth2;
    public List<Integer> intList;
    public String stringDepth1;


}
