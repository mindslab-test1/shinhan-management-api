package ai.maum.shinhan_management_api.api.test.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class TestDataHandleDto {

    public DataHandleDepth1 dataHandleDepth1;
    public List<String> stringsList;
    public TestDataHandleDto() {
    }
}
