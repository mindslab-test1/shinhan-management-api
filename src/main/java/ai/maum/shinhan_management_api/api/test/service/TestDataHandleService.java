package ai.maum.shinhan_management_api.api.test.service;

import ai.maum.shinhan_management_api.api.test.dto.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class TestDataHandleService {


    public TestDataHandleDto dataHandle(){

        TestDataHandleDto responseResult = new TestDataHandleDto();

        List<String> strListDataDataHandleDto = new ArrayList<>();
        strListDataDataHandleDto.add("test1");
        strListDataDataHandleDto.add("test2");
        strListDataDataHandleDto.add("test3");

        DataHandleDepth1 dataHandleDepth1 = new DataHandleDepth1();
        List<Integer> intListDataDataHandleDepth1 = new ArrayList<>();
        intListDataDataHandleDepth1.add(1);
        intListDataDataHandleDepth1.add(2);
        intListDataDataHandleDepth1.add(3);

        dataHandleDepth1.setIntList(intListDataDataHandleDepth1);
        dataHandleDepth1.setStringDepth1("테스트입니다.");

        responseResult.setStringsList(strListDataDataHandleDto);
        responseResult.setDataHandleDepth1(dataHandleDepth1);

        //dataHandleDepth1.setDataHandleDepth2();

        List<String> strListDataDataHandleDto2 = new ArrayList<>();
        strListDataDataHandleDto2.add("test4");
        strListDataDataHandleDto2.add("test5");
        strListDataDataHandleDto2.add("test6");

        DataHandleDepth2 dataHandleDepth2 = new DataHandleDepth2();
        List<Integer> intListDataDataHandleDepth2 = new ArrayList<>();
        intListDataDataHandleDepth2.add(4);
        intListDataDataHandleDepth2.add(5);
        intListDataDataHandleDepth2.add(6);

        dataHandleDepth2.setIntList2(intListDataDataHandleDepth2);
        dataHandleDepth2.setStringDepth2("테스트입니다2");

        dataHandleDepth1.setDataHandleDepth2(dataHandleDepth2);

        List<String> strListDataDataHandleDto3 = new ArrayList<>();
        strListDataDataHandleDto3.add("test7");
        strListDataDataHandleDto3.add("test8");
        strListDataDataHandleDto3.add("test9");

        DataHandleDepth3 dataHandleDepth3 = new DataHandleDepth3();
        List<Integer> intListDataDataHandleDepth3 = new ArrayList<>();
        intListDataDataHandleDepth3.add(7);
        intListDataDataHandleDepth3.add(8);
        intListDataDataHandleDepth3.add(9);

        DataHandleDepth4 dataHandleDepth4 = new DataHandleDepth4();
        dataHandleDepth3.setIntList3(intListDataDataHandleDepth3);
        dataHandleDepth3.setDataHandleDepth4(dataHandleDepth4);
        /////////////////////////////////////////////





        return responseResult;
    }
}
