package ai.maum.shinhan_management_api.api.test.dto;

import lombok.Data;

import java.util.List;

@Data
public class DataHandleDepth3 {

    public DataHandleDepth4 dataHandleDepth4;
    public List<Integer> intList3;
    public String stringDepth3;
}
