package ai.maum.shinhan_management_api.api.test.dto;

import lombok.Data;

import java.util.List;

@Data
public class DataHandleDepth2 {
    public DataHandleDepth3 dataHandleDepth3;
    public List<Integer> intList2;
    public String stringDepth2;
}
