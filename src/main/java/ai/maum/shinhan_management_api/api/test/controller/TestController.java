package ai.maum.shinhan_management_api.api.test.controller;

import ai.maum.shinhan_management_api.api.test.dto.TestDataHandleDto;
import ai.maum.shinhan_management_api.api.test.service.TestDataHandleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/test")
public class TestController {

    @Autowired
    TestDataHandleService testDataHandle;

    @PostMapping("/data/handle")
    public TestDataHandleDto userSearch(){
        return testDataHandle.dataHandle();
    }
}
