package ai.maum.shinhan_management_api.api.test.dto;

import lombok.Data;

import java.util.List;

@Data
public class DataHandleDepth4 {

    public List<Integer> intList4;
    public String stringDepth4;
}
