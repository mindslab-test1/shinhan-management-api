package ai.maum.shinhan_management_api.api.device.dto;

import lombok.Data;

@Data
public class ConciergeStandbyUploadRequestDto {
    private byte[] data;
    private String name;
    private String extension;
    private String imageYn;
}
