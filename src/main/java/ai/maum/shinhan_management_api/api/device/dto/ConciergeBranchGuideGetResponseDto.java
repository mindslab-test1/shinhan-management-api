package ai.maum.shinhan_management_api.api.device.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ConciergeBranchGuideGetResponseDto {
    private List<GuideData> guideDataList;

    @Data
    @Builder
    public static class GuideData{
        private String fileName;
        private int floor;
        private List<Text> text;

        @Data
        @Builder
        public static class Text{
            private String title;
            private String content;
        }
    }
}
