package ai.maum.shinhan_management_api.api.device.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class ConciergeStandbyListResponseDto {
    private List<Standby> standbyList;

    @Data
    @Builder
    public static class Standby {
        private int id;
        private String branchNo;
        private String deviceNo;
        private String branchName;
        private int imageCnt;
        private int videoCnt;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime created;
    }
}
