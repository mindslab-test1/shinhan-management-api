package ai.maum.shinhan_management_api.api.device.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class ConciergeBranchGuideListResponseDto {

    List<Concierge> conciergeList;

    @Data
    @Builder
    public static class Concierge {
        private int deviceId;
        private String branchName;
        private String branchNo;
        private String deviceNo;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime created;
    }
}
