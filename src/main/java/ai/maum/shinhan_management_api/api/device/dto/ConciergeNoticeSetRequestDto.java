package ai.maum.shinhan_management_api.api.device.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ConciergeNoticeSetRequestDto {
    private String branchNo;
    private String deviceNo;
    private String text;
    private String branchYn;
    private String useYn;
    private LocalDate startDate;
    private LocalDate endDate;
}
