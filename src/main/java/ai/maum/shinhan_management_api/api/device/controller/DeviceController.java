package ai.maum.shinhan_management_api.api.device.controller;

import ai.maum.shinhan_management_api.api.device.dto.*;
import ai.maum.shinhan_management_api.api.device.service.DeviceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.nio.file.Path;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/api/v1/device")
public class DeviceController {
    private final DeviceService deviceService;

    @PostMapping
    public Mono<DeviceRegisterResponseDto> registerDevice(@RequestHeader String branchNo,
                                                          @RequestHeader String deviceNo,
                                                          @RequestHeader String deviceType){
        log.info("기기 등록 요청(지점번호: " + branchNo + ", 기기번호: " + deviceNo + ", 기기종류: " + deviceType + ")");
        return deviceService.registerDevice(branchNo, deviceNo, deviceType);
    }

    @PostMapping("/concierge/standby/upload")
    public Mono<ConciergeResultResponseDto> uploadConciergeStandby(@RequestBody ConciergeStandbyUploadRequestDto req){
        log.info("컨시어지 대기화면 파일 업로드 요청");
        return deviceService.uploadConciergeStandby(req);
    }

    @PostMapping("/concierge/standby/set")
    public Mono<ConciergeResultResponseDto> setConciergeStandby(@RequestBody ConciergeStandbySetRequestDto req){
        log.info("컨시어지 대기화면 수정 요청");
        return deviceService.setConciergeStandby(req);
    }

    @PostMapping("/concierge/standby/get")
    public Mono<ConciergeStandbyGetResponseDto> getConciergeStandby(@RequestHeader String branchNo,
                                                                    @RequestHeader String deviceNo){
        log.info("컨시어지 대기화면 조회 요청");
        return deviceService.getConciergeStandby(branchNo, deviceNo);
    }

    @PostMapping("/concierge/standby/download")
    public Mono<ConciergeDownloadResponseDto> downloadConciergeStandby(@RequestHeader String branchNo,
                                                                       @RequestHeader String deviceNo){
        log.info("컨시어지 대기화면 파일 다운로드 요청");
        return deviceService.downloadConciergeStandby(branchNo, deviceNo);
    }

    @GetMapping("/concierge/standby/list/{page}")
    public Mono<ConciergeStandbyListResponseDto> listConciergeStandby(@PathVariable int page){
        log.info("컨시어지 대기화면 목록 조회 요청");
        return deviceService.listConciergeStandby(page);
    }

    @GetMapping("/concierge/standby/list/page")
    public Mono<ConciergePageResponseDto> listPageConciergeStandby(){
        log.info("컨시어지 대기화면 목록 페이지 개수 조회 요청");
        return deviceService.listPageConciergeStandby();
    }

    @GetMapping("/concierge/standby/detail/{id}")
    public Mono<ConciergeStandbyDetailResponseDto> detailConciergeStandby(@PathVariable int id){
        log.info("컨시어지 대기화면 상세 조회 요청");
        return deviceService.detailConciergeStandby(id);
    }

    @PostMapping("/concierge/tooltip/set")
    public Mono<Void> setConciergeTooltip(@RequestBody ConciergeTooltipSetRequestDto req){
        log.info("컨시어지 툴팁 등록 요청");
        return deviceService.setConciergeTooltip(req);
    }

    @PostMapping("/concierge/tooltip/get")
    public Mono<ConciergeTooltipGetResponseDto> getConciergeTooltip(@RequestHeader String branchNo,
                                                                     @RequestHeader String deviceNo){
        log.info("컨시어지 툴팁 목록 요청");
        return deviceService.getConciergeTooltip(branchNo, deviceNo);
    }

    @GetMapping("/concierge/tooltip/list")
    public Mono<ConciergeTooltipListResponseDto> listConciergeTooltip(){
        log.info("컨시어지 툴팁 목록 요청");
        return deviceService.listConciergeTooltip();
    }

    @PostMapping("/concierge/notice/set")
    public Mono<ConciergeResultResponseDto> setConciergeNotice(@RequestBody ConciergeNoticeSetRequestDto req){
        log.info("컨시어지 공지사항 등록 요청");
        return deviceService.setConciergeNotice(req);
    }

    @DeleteMapping("/concierge/notice/delete/{id}")//
    public Mono<Void> deleteConciergeNotice(@PathVariable int id){
        log.info("컨시어지 공지사항 삭제 요청");
        return deviceService.deleteConciergeNotice(id);
    }

    @PostMapping("/concierge/notice/delete/list")
    public Mono<Void> deleteConciergeNoticeList(@RequestBody ConciergeNoticeDeleteRequestDto req){
        log.info("선택된 컨시어지 공지사항 목록 삭제 요청");
        return deviceService.deleteConciergeNoticeList(req);
    }

    @PostMapping("/concierge/notice/get")
    public Mono<ConciergeNoticeGetResponseDto> getConciergeNotice(@RequestHeader String branchNo,
                                                                  @RequestHeader String deviceNo){
        log.info("컨시어지 공지사항 조회 요청(지점번호: " + branchNo + ", 기기번호: " + deviceNo + ")");
        return deviceService.getConciergeNotice(branchNo, deviceNo);
    }

    @GetMapping("/concierge/notice/list/{page}")
    public Mono<ConciergeNoticeListResponseDto> listConciergeNotice(@PathVariable int page){
        log.info("컨시어지 {} 페이지 공지사항 목록 조회 요청", page);
        return deviceService.listConciergeNotice(page);
    }

    @GetMapping("/concierge/notice/list/page")
    public Mono<ConciergePageResponseDto> listPageConciergeNotice(){
        log.info("컨시어지 공지사항 목록 페이지 개수 조회 요청");
        return deviceService.listPageConciergeNotice();
    }

    @GetMapping("/concierge/notice/detail/{id}")
    public Mono<ConciergeNoticeDetailResponseDto> detailConciergeNotice(@PathVariable int id){
        log.info("컨시어지 공지사항 상세 조회 요청");
        return deviceService.detailConciergeNotice(id);
    }

    @PutMapping("/concierge/notice/modify/{id}")
    public Mono<ConciergeResultResponseDto> modifyConciergeNotice(@PathVariable int id,
                                            @RequestBody ConciergeNoticeModifyRequestDto req){
        return deviceService.modifyConciergeNotice(id, req);
    }

    @PostMapping("/concierge/guide/download")
    public Mono<ConciergeDownloadResponseDto> downloadConciergeGuide(@RequestHeader String branchNo, @RequestHeader String deviceNo){
        log.info("컨시어지 지점안내 다운로드 요청(지점번호 : "+ branchNo +", 기기번호 : " + deviceNo +")");
        return deviceService.downloadConciergeGuide(branchNo,deviceNo);
    }

    @GetMapping("/concierge/guide/get")
    public Mono<ConciergeBranchGuideGetResponseDto> getConciergeGuide(@RequestHeader String branchNo, @RequestHeader String deviceNo){
        log.info("컨시어지 지점안내 조회 요청(지점번호 : "+ branchNo +", 기기번호 : " + deviceNo +")");
        return deviceService.getConciergeBranchGuide(branchNo , deviceNo);
    }

    @GetMapping("/concierge/guide/list/{page}")
    public Mono<ConciergeBranchGuideListResponseDto> listConciergeGuide(@PathVariable int page) {
        log.info("컨시어지 {} 페이지 지점안내 목록 조회 요청", page);
        return deviceService.listConciergeBranchGuide(page);
    }

    @GetMapping("/concierge/guide/page")
    public Mono<ConciergePageResponseDto> pageConciergeGuide() {
        log.info("컨시어지 지점안내 목록 페이지 개수 조회 요청");
        return deviceService.listPageConciergeGuide();
    }


    @GetMapping("/concierge/guide/detail/{deviceId}")
    public Mono<ConciergeBranchGuideDetailResponseDto> detailConciergeGuide(@PathVariable int deviceId) {
        log.info("컨시어지 지점안내 상세 조회 요청");
        return deviceService.detailConciergeGuide(deviceId);
    }



    @PostMapping("/concierge/guide/modify/{id}")
    public Mono<Void> modifyConciergeGuide(@PathVariable int deviceId, @RequestBody ConciergeBranchGuideModifyRequestDto requestDto) {
        log.info("컨시어지 지점안내 상세 조회 요청");
        return null;
//        return deviceService.modifyConciergeGuide(deviceId, requestDto);
    }
}
