package ai.maum.shinhan_management_api.api.device.dto;

import lombok.Data;

import java.util.List;

@Data
public class ConciergeStandbySetRequestDto {
    private String branchNo;
    private String deviceNo;
    private List<Boolean> typeList;
    private List<Standby> standbyList;

    @Data
    public static class Standby {
        private String name;
        private int textR;
        private int textG;
        private int textB;
    }
}
