package ai.maum.shinhan_management_api.api.device.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ConciergeTooltipGetResponseDto {
    private List<Tooltip> tooltipList;

    @Data
    @Builder
    public static class Tooltip {
        private int id;
        private String text;
    }
}