package ai.maum.shinhan_management_api.api.device.dto;

import lombok.Data;

import java.util.List;

@Data
public class ConciergeNoticeDeleteRequestDto {
    private List<Integer> noticeIdList;
}
