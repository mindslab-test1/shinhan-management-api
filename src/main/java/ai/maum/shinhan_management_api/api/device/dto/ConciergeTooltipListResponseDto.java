package ai.maum.shinhan_management_api.api.device.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class ConciergeTooltipListResponseDto {
    private List<ConciergeTooltip> tooltipList;

    @Data
    @Builder
    public static class ConciergeTooltip {
        private String text;;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime created;

    }
}