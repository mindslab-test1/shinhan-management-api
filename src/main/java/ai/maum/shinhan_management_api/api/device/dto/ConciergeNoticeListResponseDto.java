package ai.maum.shinhan_management_api.api.device.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class ConciergeNoticeListResponseDto {
    private List<Notice> noticeList;

    @Data
    @Builder
    public static class Notice {
        private int id;
        private String branchYn;
        private String branchName;
        private String branchNo;
        private String deviceNo;
        private String text;
        private String useYn;
        @JsonFormat(pattern = "yyyy-MM-dd")
        private LocalDate startDate;
        @JsonFormat(pattern = "yyyy-MM-dd")
        private LocalDate endDate;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime created;
    }
}
