package ai.maum.shinhan_management_api.api.device.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class ConciergeBranchGuideDetailResponseDto {
    private String branchName;
    private String branchNo;
    private String deviceNo;
    private List<Guide> guideList;

    @Data
    @Builder
    public static class Guide {
        private int id;
        private int floor;
        private String image;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private LocalDateTime created;
        private List<Text> text;

        @Data
        @Builder
        public static class Text {
            private int id;
            private String title;
            private String content;
        }
    }
}

