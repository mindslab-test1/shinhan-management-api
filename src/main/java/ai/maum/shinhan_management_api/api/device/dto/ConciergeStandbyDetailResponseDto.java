package ai.maum.shinhan_management_api.api.device.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ConciergeStandbyDetailResponseDto {
    private List<Boolean> typeList;
    private List<Standby> standbyList;

    @Data
    @Builder
    public static class Standby {
        private String imageYn;
        private String name;
        private String textRGB;
    }
}
