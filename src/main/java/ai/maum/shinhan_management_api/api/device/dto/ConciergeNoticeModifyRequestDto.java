package ai.maum.shinhan_management_api.api.device.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ConciergeNoticeModifyRequestDto {
    private String text;
    private LocalDate startDate;
    private LocalDate endDate;
    private String useYn;
}
