package ai.maum.shinhan_management_api.api.device.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ConciergeDownloadResponseDto {
    private List<File> fileList;

    @Data
    @Builder
    public static class File {
        private byte[] data;
        private String name;
    }
}
