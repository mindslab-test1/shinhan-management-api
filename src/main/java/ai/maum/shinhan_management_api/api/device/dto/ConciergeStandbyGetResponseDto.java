package ai.maum.shinhan_management_api.api.device.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ConciergeStandbyGetResponseDto {
    private List<Boolean> typeList;
    private List<Standby> standbyList;

    @Data
    @Builder
    public static class Standby {
        private String fileName;
        private int textR;
        private int textG;
        private int textB;
    }
}
