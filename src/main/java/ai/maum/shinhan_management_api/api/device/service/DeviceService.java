package ai.maum.shinhan_management_api.api.device.service;

import ai.maum.shinhan_management_api.api.device.dto.*;
import ai.maum.shinhan_management_api.common.Common;
import ai.maum.shinhan_management_api.entity.*;
import ai.maum.shinhan_management_api.exception.ConciergeNoticeOverflowException;
import ai.maum.shinhan_management_api.exception.ConciergeStandbyFileExistException;
import ai.maum.shinhan_management_api.repository.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.io.ByteArrayOutputStream;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class DeviceService {
    private final DeviceRepository deviceRepository;
    private final BranchRepository branchRepository;
    private final AiHumanRepository aiHumanRepository;
    private final DeviceTypeRepository deviceTypeRepository;
    private final VideoRepository videoRepository;
    private final VideoDistributionGroupRepo videoDistributionGroupRepo;
    private final VideoDistributionRepository videoDistributionRepository;
    private final ConciergeNoticeRepository conciergeNoticeRepository;
    private final ConciergeStandbyRepository conciergeStandbyRepository;
    private final ConciergeStandbyConfigRepository conciergeStandbyConfigRepository;
    private final ConciergeStandbyFileRepository conciergeStandbyFileRepository;
    private final ConciergeTooltipRepository conciergeTooltipRepository;
    private final ConciergeBranchGuideRepository conciergeBranchGuideRepository;
    private final ConciergeBranchGuideTextRepository conciergeBranchGuideTextRepository;

    @Value("${concierge.standby.file.dir}")
    private String conciergeStandbyFileDirectory;

    @Value("${concierge.guide.file.dir}")
    private String conciergeGuideFileDirectory;

    public Mono<DeviceRegisterResponseDto> registerDevice(String branchNo,
                                                          String deviceNo,
                                                          String deviceType){
        return deviceTypeRepository.findByCode(deviceType)
                .flatMap(dt ->
                        deviceRepository.findByBranchNoAndDeviceNoAndDeviceType(branchNo, deviceNo, dt.getId())
                                .hasElement()
                                .flatMap(hasElement -> {
                                    if(hasElement){
                                        log.info("기기가 이미 등록되어 있음(지점번호: " + branchNo + ", 기기번호: " + deviceNo + ")");
                                        return Mono.just(DeviceRegisterResponseDto.builder()
                                                .result("기기가 이미 등록되어 있습니다.")
                                                .build());
                                    }

                                    return deviceRepository.save(Device.builder()
                                                    .deviceType(dt.getId())
                                                    .deviceNo(deviceNo)
                                                    .branchNo(branchNo)
                                                    .scenarioVersion(2)
                                                    .build())
                                            .flatMap(device -> {
                                                log.info("기기 등록 완료(지점번호: " + branchNo + ", 기기번호: " + deviceNo + ")");

                                                int engine = 4;
                                                int background = 2;
                                                int resolution = 8;

                                                switch (device.getDeviceType()){
                                                    case 1:
                                                    case 3:
                                                    case 4:
                                                        engine = 4;
                                                        background = 2;
                                                        resolution = 8;
                                                        break;
                                                    case 2:
                                                        engine = 2;
                                                        background = 1;
                                                        resolution = 3;
                                                        break;
                                                }

                                                return aiHumanRepository.save(AiHuman.builder()
                                                        .uuid(UUID.randomUUID().toString())
                                                        .device(device.getId())
                                                        .engine(engine)
                                                        .avatar(2)
                                                        .outfit(3)
                                                        .background(background)
                                                        .resolution(resolution)
                                                        .scenarioVersion(2)
                                                        .build())
                                                        .flatMap(aiHuman ->
                                                                deviceRepository.save(device.toBuilder()
                                                                        .aiHuman(aiHuman.getId())
                                                                        .aiHumanUuid("82-"+device.getBranchNo()+"-"+device.getDeviceNo()+"-"+aiHuman.getUuid())
                                                                        .registerStatus(1)
                                                                        .build())
                                                                        .flatMap(_device ->
                                                                                videoRepository
                                                                                        .findByEngineAndAvatarAndOutfitAndBackgroundAndResolutionAndScenarioVersionAndDeviceType(
                                                                                                aiHuman.getEngine(),
                                                                                                aiHuman.getAvatar(),
                                                                                                aiHuman.getOutfit(),
                                                                                                aiHuman.getBackground(),
                                                                                                aiHuman.getResolution(),
                                                                                                aiHuman.getScenarioVersion(),
                                                                                                device.getDeviceType()
                                                                                        ).collectList()
                                                                                        .flatMap(videos ->
                                                                                                videoDistributionGroupRepo.save(VideoDistributionGroup.builder()
                                                                                                                .device(device.getId())
                                                                                                                .totalCnt(videos.size())
                                                                                                                .status(4)
                                                                                                                .build())
                                                                                                        .flatMapMany(group ->
                                                                                                                videoDistributionRepository.saveAll(videos.stream()
                                                                                                                        .map(video -> VideoDistribution.builder()
                                                                                                                                .video(video.getId())
                                                                                                                                .groupId(group.getId())
                                                                                                                                .scenarioVersion(video.getScenarioVersion())
                                                                                                                                .status(4)
                                                                                                                                .build())
                                                                                                                        .collect(Collectors.toList())))
                                                                                                        .then(Mono.just(DeviceRegisterResponseDto.builder()
                                                                                                                .result("기기를 등록하였습니다.")
                                                                                                                .build())))));
                                            })
                                            .doOnError(error -> {
                                                log.error("기기 등록 오류(지점번호: " + branchNo + ", 기기번호: " + deviceNo + ")");
                                                log.error(error.getMessage());
                                            })
                                            .onErrorReturn(DeviceRegisterResponseDto.builder()
                                                    .result("기기 등록에 오류가 발생하였습니다.")
                                                    .build());
                                }));
    }

    public Mono<ConciergeResultResponseDto> uploadConciergeStandby(ConciergeStandbyUploadRequestDto req){
        DataBuffer buffer = new DefaultDataBufferFactory().wrap(req.getData());
        String fileName = req.getName()
                + "_"
                + new Timestamp(System.currentTimeMillis())
                + "."
                + req.getExtension();

        return conciergeStandbyFileRepository.existsByName(req.getName())
                .flatMap(isExist -> {
                    if(isExist) return Mono.error(new ConciergeStandbyFileExistException());
                    return DataBufferUtils.write(Mono.just(buffer), Paths.get(conciergeStandbyFileDirectory+fileName));
                })
                .switchIfEmpty(Mono.defer(() -> {
                            DataBufferUtils.release(buffer);
                            return conciergeStandbyFileRepository.save(ConciergeStandbyFile.builder()
                                    .name(req.getName())
                                    .imageYn(req.getImageYn())
                                    .file(fileName)
                                    .size(req.getData().length)
                                    .build());
                        })
                        .doOnNext(result -> log.info("컨시어지 대기화면 파일을 업로드하였습니다."))
                        .then())
                .then(Mono.just(ConciergeResultResponseDto.builder()
                        .code(0)
                        .message("컨시어지 대기화면 파일 업로드: 성공")
                        .build()))
                .onErrorResume(error -> {
                    if(error.getClass().getSimpleName().equals("ConciergeStandbyFileExistException")){
                        log.error("이미 존재하는 컨시어지 대기화면 파일입니다.");
                        return Mono.just(ConciergeResultResponseDto.builder()
                                .code(2)
                                .message("컨시어지 대기화면 파일 업로드: 실패(동일 파일명 존재)")
                                .build());
                    }

                    log.error("컨시어지 대기화면 파일 업로드 중 오류가 발생하였습니다.");

                    return Mono.just(ConciergeResultResponseDto.builder()
                            .code(1)
                            .message("컨시어지 대기화면 파일 업로드: 실패(시스템 오류)")
                            .build());
                });
    }

    public Mono<ConciergeResultResponseDto> setConciergeStandby(ConciergeStandbySetRequestDto req){
        if(req.getStandbyList().size()>Common.CONCIERGE_STANDBY_MAX)
            return Mono.just(ConciergeResultResponseDto.builder()
                    .code(2)
                    .message("컨시어지 대기화면 수정: 실패(최대 개수 초과)")
                    .build());

        return deviceRepository.findByBranchNoAndDeviceNo(req.getBranchNo(), req.getDeviceNo())
                .flatMap(device -> conciergeStandbyConfigRepository.findByDevice(device.getId())
                        .flatMap(standbyConfig -> conciergeStandbyRepository.findByConfigId(standbyConfig.getId())
                                .collectList()
                                .flatMap(standbyList -> conciergeStandbyRepository.deleteAll(standbyList)
                                        .then(Mono.defer(() -> {
                                            standbyConfig.setTypeA(req.getTypeList().get(0));
                                            standbyConfig.setTypeB(req.getTypeList().get(1));
                                            standbyConfig.setTypeC(req.getTypeList().get(2));
                                            return conciergeStandbyConfigRepository.save(standbyConfig);
                                        }))
                                        .flatMapMany(sc -> Flux.fromIterable(req.getStandbyList()))
                                        .index()
                                        .flatMapSequential(standbyInfo -> conciergeStandbyFileRepository
                                                .findByName(standbyInfo.getT2().getName())
                                                .flatMap(sf -> conciergeStandbyRepository.save(ConciergeStandby.builder()
                                                        .configId(standbyConfig.getId())
                                                        .fileId(sf.getId())
                                                        .textR(standbyInfo.getT2().getTextR())
                                                        .textG(standbyInfo.getT2().getTextG())
                                                        .textB(standbyInfo.getT2().getTextB())
                                                        .position((int)(standbyInfo.getT1()+1))
                                                        .build())))
                                        .then(Mono.just(ConciergeResultResponseDto.builder()
                                                .code(0)
                                                .message("컨시어지 대기화면 수정: 성공")
                                                .build())))))
                .onErrorReturn(ConciergeResultResponseDto.builder()
                        .code(1)
                        .message("컨시어지 대기화면 수정: 실패(시스템 오류)")
                        .build());
    }

    public Mono<ConciergeStandbyGetResponseDto> getConciergeStandby(String branchNo, String deviceNo){
        return deviceRepository.findByBranchNoAndDeviceNo(branchNo, deviceNo)
                .flatMap(device -> conciergeStandbyConfigRepository.findByDevice(device.getId())
                        .flatMap(standbyConfig -> conciergeStandbyRepository
                                .findByConfigIdOrderByPosition(standbyConfig.getId())
                                .flatMapSequential(standby -> conciergeStandbyFileRepository.findById(standby.getFileId())
                                        .map(standbyFile -> ConciergeStandbyGetResponseDto.Standby.builder()
                                                .fileName(standbyFile.getFile())
                                                .textR(standby.getTextR())
                                                .textG(standby.getTextG())
                                                .textB(standby.getTextB())
                                                .build()))
                                .collectList()
                                .map(standbyList -> ConciergeStandbyGetResponseDto.builder()
                                        .typeList(List.of(standbyConfig.isTypeA(),
                                                standbyConfig.isTypeB(),
                                                standbyConfig.isTypeC()))
                                        .standbyList(standbyList)
                                        .build())))
                .doOnNext(result -> log.info("컨시어지 대기화면을 조회하였습니다."))
                .doOnError(error -> {
                    log.error("컨시어지 대기화면 조회 중 오류가 발생하였습니다.");
                    log.error(error.getMessage());
                });
    }

    public Mono<ConciergeDownloadResponseDto> downloadConciergeStandby(String branchNo, String deviceNo) {
        return deviceRepository.findByBranchNoAndDeviceNo(branchNo, deviceNo)
                .flatMap(device -> conciergeStandbyConfigRepository.findByDevice(device.getId())
                        .flatMap(standbyConfig -> conciergeStandbyRepository.findByConfigId(standbyConfig.getId())
                                .flatMap(standby -> conciergeStandbyFileRepository.findById(standby.getFileId())
                                        .flatMap(standbyFile -> DataBufferUtils.read(Paths
                                                                .get(conciergeStandbyFileDirectory+standbyFile.getFile()),
                                                        new DefaultDataBufferFactory(), Common.CONCIERGE_FILE_BUFFER_SIZE)
                                                .map(buffer -> {
                                                    byte[] bytes = new byte[buffer.readableByteCount()];
                                                    buffer.read(bytes);
                                                    DataBufferUtils.release(buffer);
                                                    return bytes;
                                                })
                                                .collectList()
                                                .map(bytesList -> bytesList.stream().collect(() -> new ByteArrayOutputStream(),
                                                        (os, bytes) -> os.write(bytes, 0, bytes.length),
                                                        (os1, os2) -> {}).toByteArray())
                                                .map(bytes -> ConciergeDownloadResponseDto.File.builder()
                                                        .data(bytes)
                                                        .name(standbyFile.getFile())
                                                        .build())
                                        ))
                                .collectList()
                                .map(fileList -> ConciergeDownloadResponseDto.builder()
                                        .fileList(fileList)
                                        .build())
                                .doOnNext(result -> log.info("컨시어지 대기화면 파일을 다운로드하였습니다."))
                                .doOnError(error -> {
                                    log.error("컨시어지 대기화면 파일 다운로드 중 오류가 발생하였습니다.");
                                    log.error(error.getMessage());
                                })));
    }

    public Mono<ConciergeStandbyListResponseDto> listConciergeStandby(int page){
        return deviceRepository.findByDeviceTypeOrderByCreatedDescIdDesc(1,
                        PageRequest.of(page-1, Common.CONCIERGE_STANDBY_LIST_PER_PAGE))
                .flatMapSequential(device -> branchRepository.findByBranchNo(device.getBranchNo())
                        .flatMapMany(branch -> conciergeStandbyConfigRepository.findByDevice(device.getId())
                                .flatMap(standbyConfig -> conciergeStandbyRepository.findByConfigId(standbyConfig.getId())
                                        .flatMap(standby -> conciergeStandbyFileRepository.findById(standby.getFileId()))
                                        .groupBy(ConciergeStandbyFile::getImageYn)
                                        .flatMap(group -> group.collectList()
                                                .map(standbyFileList -> Map.of(standbyFileList.get(0).getImageYn(),
                                                        standbyFileList.size())))
                                        .collectList()
                                        .map(cntMapList -> {
                                            int imageCnt = 0;
                                            int videoCnt = 0;

                                            for(Map<String, Integer> cntMap: cntMapList){
                                                if(cntMap.containsKey("Y")) imageCnt = cntMap.get("Y");
                                                else videoCnt = cntMap.get("N");
                                            }

                                            return ConciergeStandbyListResponseDto.Standby.builder()
                                                    .id(device.getId())
                                                    .branchNo(device.getBranchNo())
                                                    .deviceNo(device.getDeviceNo())
                                                    .branchName(branch.getBranchName())
                                                    .imageCnt(imageCnt)
                                                    .videoCnt(videoCnt)
                                                    .created(device.getCreated())
                                                    .build();
                                        }))
                                .switchIfEmpty(Mono.just(ConciergeStandbyListResponseDto.Standby.builder()
                                        .id(device.getId())
                                        .branchNo(device.getBranchNo())
                                        .deviceNo(device.getDeviceNo())
                                        .branchName(branch.getBranchName())
                                        .imageCnt(0)
                                        .videoCnt(0)
                                        .created(device.getCreated())
                                        .build()))))
                .collectList()
                .map(standbyList -> ConciergeStandbyListResponseDto.builder()
                        .standbyList(standbyList)
                        .build())
                .doOnNext(result -> log.info("컨시어지 대기화면 목록을 조회하였습니다."))
                .doOnError(error -> {
                    log.error("컨시어지 대기화면 목록 조회 중 오류가 발생하였습니다.");
                    log.error(error.getMessage());
                });
    }

    public Mono<ConciergePageResponseDto> listPageConciergeStandby(){
        return deviceRepository.findByDeviceType(1).count()
                .map(count -> {
                    int pageCnt = (int)(count/Common.CONCIERGE_STANDBY_LIST_PER_PAGE);

                    if((int)(count%Common.CONCIERGE_STANDBY_LIST_PER_PAGE)!=0)
                        pageCnt += 1;

                    return ConciergePageResponseDto.builder()
                            .pageCnt(pageCnt)
                            .build();
                })
                .doOnNext(result -> log.info("컨시어지 대기화면 목록 페이지 개수를 조회하였습니다."))
                .doOnError(error -> {
                    log.error("컨시어지 대기화면 목록 페이지 개수 조회 중 오류가 발생하였습니다.");
                    log.error(error.getMessage());
                });
    }

    public Mono<ConciergeStandbyDetailResponseDto> detailConciergeStandby(int id){
        return conciergeStandbyConfigRepository.findByDevice(id)
                .flatMap(standbyConfig -> conciergeStandbyRepository.findByConfigIdOrderByPosition(standbyConfig.getId())
                        .flatMapSequential(standby -> conciergeStandbyFileRepository.findById(standby.getFileId())
                                .map(standbyFile -> ConciergeStandbyDetailResponseDto.Standby.builder()
                                        .imageYn(standbyFile.getImageYn())
                                        .name(standbyFile.getName())
                                        .textRGB(standby.getTextR()+","+standby.getTextG()+","+standby.getTextB())
                                        .build()))
                        .collectList()
                        .map(standbyList -> ConciergeStandbyDetailResponseDto.builder()
                                .typeList(List.of(standbyConfig.isTypeA(), standbyConfig.isTypeB(), standbyConfig.isTypeC()))
                                .standbyList(standbyList)
                                .build()))
                .doOnNext(result -> log.info("컨시어지 대기화면을 상세 조회하였습니다."))
                .doOnError(error -> {
                    log.error("컨시어지 대기화면 상세 조회 중 오류가 발생하였습니다.");
                    log.error(error.getMessage());
                });
    }

    public Mono<Void> setConciergeTooltip(ConciergeTooltipSetRequestDto req){
        log.info("컨시어지 툴팁 등록 요청");

        conciergeTooltipRepository.deleteAll()
                .publishOn(Schedulers.boundedElastic())
                .switchIfEmpty(Mono.defer(() -> {
                    for(int i=0; i<req.getTextList().size(); i++){
                        conciergeTooltipRepository.save(
                                ConciergeTooltip
                                        .builder()
                                        .text(req.getTextList().get(i))
                                        .build()).block();

                    }

                    return Mono.empty();
                })).subscribe();

        return Mono.empty();
    }

    public Mono<ConciergeTooltipGetResponseDto> getConciergeTooltip(String branchNo,
                                                                    String deviceNo){

        log.info("컨시어지 툴 팁 목록 요청(지점번호: " + branchNo + ", 기기번호: " + deviceNo + ")");

        List<ConciergeTooltipGetResponseDto.Tooltip> getConciergeTooltip = new ArrayList<>();

        return  conciergeTooltipRepository.findAll()
                .flatMapSequential(tooltip -> {
                    getConciergeTooltip.add(ConciergeTooltipGetResponseDto
                            .Tooltip.builder()
                            .id(tooltip.getId())
                            .text(tooltip.getText())
                            .build());
                    return Mono.just(tooltip);
                }).then(Mono.just(ConciergeTooltipGetResponseDto
                        .builder()
                        .tooltipList(getConciergeTooltip)
                        .build()));
    }

    public Mono<ConciergeTooltipListResponseDto> listConciergeTooltip(){
        log.info("컨시어지 툴팁 리스트 요청");

        List<ConciergeTooltipListResponseDto.ConciergeTooltip> listConciergeTooltip = new ArrayList<>();

        return conciergeTooltipRepository.findAllByOrderByIdAsc()
                .flatMapSequential(tooltip -> {
                    listConciergeTooltip.add(ConciergeTooltipListResponseDto
                            .ConciergeTooltip.builder()
                            .text(tooltip.getText())
                            .created(tooltip.getCreated())
                            .build());
                    System.out.println("find : " + tooltip);
                    return Mono.just(tooltip);

                }).then(Mono.just(ConciergeTooltipListResponseDto
                        .builder()
                        .tooltipList(listConciergeTooltip)
                        .build()));
    }

    public Mono<ConciergeResultResponseDto> setConciergeNotice(ConciergeNoticeSetRequestDto req){
        if(req.getBranchYn().equals("N"))
            return conciergeNoticeRepository.countByDeviceAndBranchYnAndUseYn(null, req.getBranchYn(), "Y")
                    .flatMap(count -> {
                        log.debug("현재 등록되어 있는 컨시어지 전체 공지사항 개수: {}", count);
                        return registerConciergeNotice(req, (int)(count+1), null);
                    })
                    .doOnError(error -> {
                        log.error("컨시어지 전체 공지사항 등록 중 오류가 발생하였습니다.");
                        log.error(error.getMessage());
                    })
                    .onErrorReturn(ConciergeResultResponseDto.builder()
                            .code(1)
                            .message("컨시어지 공지사항 등록: 실패(시스템 오류)")
                            .build());

        return deviceRepository.findByBranchNoAndDeviceNo(req.getBranchNo(), req.getDeviceNo())
                .flatMap(device -> conciergeNoticeRepository
                        .countByDeviceAndBranchYnAndUseYn(device.getId(), req.getBranchYn(), "Y")
                        .flatMap(count -> {
                            log.debug("현재 등록되어 있는 컨시어지 지점({}:{}) 공지사항 개수: {}", req.getBranchNo(), req.getDeviceNo(), count);

                            if(count<Common.CONCIERGE_NOTICE_BRANCH_MAX || req.getUseYn().equals("N"))
                                return registerConciergeNotice(req, (int)(count+1), device.getId());

                            log.info("컨시어지 지점 공지사항 최대 개수({})를 초과하여 등록할 수 없습니다.", Common.CONCIERGE_NOTICE_BRANCH_MAX);

                            return Mono.just(ConciergeResultResponseDto.builder()
                                    .code(2)
                                    .message("컨시어지 공지사항 등록: 실패(최대 개수 초과)")
                                    .build());
                        })
                        .doOnError(error -> {
                            log.error("컨시어지 지점 공지사항 등록 중 오류가 발생하였습니다.");
                            log.error(error.getMessage());
                        })
                        .onErrorReturn(ConciergeResultResponseDto.builder()
                                .code(1)
                                .message("컨시어지 공지사항 등록: 실패(시스템 오류)")
                                .build()));
    }

    public Mono<Void> deleteConciergeNotice(int id){
        return conciergeNoticeRepository.findById(id)
                .flatMap(target -> {
                    if(target.getBranchYn().equals("N"))
                        return conciergeNoticeRepository.deleteById(id)
                                .switchIfEmpty(Mono.defer(() -> {
                                    log.info("컨시어지 전체 공지사항을 삭제하였습니다.");
                                    if(target.getUseYn().equals("N")) return Mono.empty();
                                    return updatePositionAfterDeleteForNotice(target);
                                }))
                                .doOnError(error -> {
                                    log.error("컨시어지 전체 공지사항 삭제 중 오류가 발생하였습니다.");
                                    log.error(error.getMessage());
                                });

                    return conciergeNoticeRepository.deleteById(id)
                            .switchIfEmpty(Mono.defer(() -> {
                                log.info("컨시어지 지점 공지사항을 삭제하였습니다.");

                                if(target.getUseYn().equals("N")
                                        || target.getPosition()==Common.CONCIERGE_NOTICE_BRANCH_MAX)
                                    return Mono.empty();

                                return updatePositionAfterDeleteForNotice(target);
                            }))
                            .doOnError(error -> {
                                log.error("컨시어지 지점 공지사항 삭제 중 오류가 발생하였습니다.");
                                log.error(error.getMessage());
                            });
                });
    }

    public Mono<Void> deleteConciergeNoticeList(ConciergeNoticeDeleteRequestDto req){
        return conciergeNoticeRepository.findAllById(req.getNoticeIdList())
                .map(notice -> {
                    if(notice.getDevice()==null) return -1;
                    return notice.getDevice();
                })
                .distinct()
                .collectList()
                .flatMap(targetDeviceList -> conciergeNoticeRepository.deleteAllById(req.getNoticeIdList())
                        .switchIfEmpty(Mono.defer(() -> {
                            log.info("선택된 컨시어지 공지사항 목록을 삭제하였습니다.");

                            if(!targetDeviceList.contains(-1))
                                return conciergeNoticeRepository.findAfterDelete(targetDeviceList)
                                        .groupBy(n -> n.getDevice())
                                        .flatMap(group -> group.sort((n1, n2) -> comparePositionForNotice(n1, n2))
                                                .index()
                                                .flatMap(noticeInfo -> {
                                                    noticeInfo.getT2().setPosition((int)(noticeInfo.getT1()+1));
                                                    return conciergeNoticeRepository.save(noticeInfo.getT2());
                                                })).then();

                            return conciergeNoticeRepository.findWithFullAfterDelete(targetDeviceList)
                                    .map(n -> {
                                        if(n.getDevice()==null) n.setDevice(-1);
                                        return n;
                                    })
                                    .groupBy(n -> n.getDevice())
                                    .flatMap(group -> group.sort((n1, n2) -> comparePositionForNotice(n1, n2))
                                            .index()
                                            .flatMap(noticeInfo -> {
                                                if(noticeInfo.getT2().getDevice()==-1) noticeInfo.getT2().setDevice(null);
                                                noticeInfo.getT2().setPosition((int)(noticeInfo.getT1()+1));
                                                return conciergeNoticeRepository.save(noticeInfo.getT2());
                                            })).then();
                        }))
                        .doOnError(error -> {
                            log.error("선택된 컨시어지 공지사항 목록 삭제 중 오류가 발생하였습니다.");
                            log.error(error.getMessage());
                        }));

    }

    public Mono<ConciergeNoticeGetResponseDto> getConciergeNotice(String branchNo, String deviceNo){
        return deviceRepository.findByBranchNoAndDeviceNo(branchNo, deviceNo)
                .flatMap(device -> conciergeNoticeRepository
                        .findByDeviceAndBranchYnAndUseYnOrderByPositionAsc(device.getId(), "Y", "Y")
                        .map(branchNotice -> ConciergeNoticeGetResponseDto.BranchNotice.builder()
                                .text(branchNotice.getText())
                                .build())
                        .collectList()
                        .map(branchNoticeList -> ConciergeNoticeGetResponseDto.builder()
                                .branchNoticeList(branchNoticeList)
                                .build())
                        .flatMap(noticeList -> conciergeNoticeRepository
                                .findByDeviceAndBranchYnAndUseYnOrderByPositionAsc(null, "N", "Y")
                                .map(fullNotice -> ConciergeNoticeGetResponseDto.FullNotice.builder()
                                        .text(fullNotice.getText())
                                        .build())
                                .collectList()
                                .flatMap(fullNoticeList -> {
                                    noticeList.setFullNoticeList(fullNoticeList);
                                    return Mono.just(noticeList);
                                })))
                .switchIfEmpty(Mono.defer(() -> conciergeNoticeRepository
                        .findByDeviceAndBranchYnAndUseYnOrderByPositionAsc(null, "N", "Y")
                        .map(fullNotice -> ConciergeNoticeGetResponseDto.FullNotice.builder()
                                .text(fullNotice.getText())
                                .build())
                        .collectList()
                        .map(fullNoticeList -> ConciergeNoticeGetResponseDto.builder()
                                .fullNoticeList(fullNoticeList)
                                .build())))
                .doOnNext(noticeList -> log.info("컨시어지 공지사항을 조회하였습니다."))
                .doOnError(error -> {
                    log.error("컨시어지 공지사항 조회 중 오류가 발생하였습니다.");
                    log.error(error.getMessage());
                });
    }

    public Mono<ConciergeNoticeListResponseDto> listConciergeNotice(int page){
        return conciergeNoticeRepository.findByOrderByCreatedDescIdDesc(PageRequest.of(page-1, Common.CONCIERGE_NOTICE_LIST_PER_PAGE))
                .flatMapSequential(notice -> {
                    if(notice.getBranchYn().equals("N"))
                        return Mono.just(ConciergeNoticeListResponseDto.Notice.builder()
                                .id(notice.getId())
                                .branchYn(notice.getBranchYn())
                                .branchName("-")
                                .branchNo("-")
                                .deviceNo("-")
                                .text(notice.getText())
                                .useYn(notice.getUseYn())
                                .startDate(notice.getStartDate())
                                .endDate(notice.getEndDate())
                                .created(notice.getCreated())
                                .build());

                    return deviceRepository.findById(notice.getDevice())
                            .flatMap(device -> branchRepository.findByBranchNo(device.getBranchNo())
                                    .map(branch -> ConciergeNoticeListResponseDto.Notice.builder()
                                            .id(notice.getId())
                                            .branchYn(notice.getBranchYn())
                                            .branchName(branch.getBranchName())
                                            .branchNo(device.getBranchNo())
                                            .deviceNo(device.getDeviceNo())
                                            .text(notice.getText())
                                            .useYn(notice.getUseYn())
                                            .startDate(notice.getStartDate())
                                            .endDate(notice.getEndDate())
                                            .created(notice.getCreated())
                                            .build()));
                })
                .collectList()
                .map(noticeList -> ConciergeNoticeListResponseDto.builder()
                        .noticeList(noticeList)
                        .build())
                .doOnNext(result -> log.info("컨시어지 {} 페이지 공지사항 목록을 조회하였습니다.", page))
                .doOnError(error -> {
                    log.error("컨시어지 공지사항 목록 조회 중 오류가 발생하였습니다.");
                    log.error(error.getMessage());
                });
    }

    public Mono<ConciergePageResponseDto> listPageConciergeNotice(){
        return conciergeNoticeRepository.count()
                .map(count -> {
                    int pageCnt = (int)(count/Common.CONCIERGE_NOTICE_LIST_PER_PAGE);

                    if((int)(count%Common.CONCIERGE_NOTICE_LIST_PER_PAGE)!=0)
                        pageCnt += 1;

                    return ConciergePageResponseDto.builder()
                            .pageCnt(pageCnt)
                            .build();
                })
                .doOnNext(result -> log.info("컨시어지 공지사항 목록 페이지 개수를 조회하였습니다."))
                .doOnError(error -> {
                    log.error("컨시어지 공지사항 목록 페이지 개수 조회 중 오류가 발생하였습니다.");
                    log.error(error.getMessage());
                });
    }

    public Mono<ConciergeNoticeDetailResponseDto> detailConciergeNotice(int id){
        return conciergeNoticeRepository.findById(id)
                .flatMap(notice -> {
                    if(notice.getBranchYn().equals("N"))
                        return Mono.just(ConciergeNoticeDetailResponseDto.builder()
                                .branchYn(notice.getBranchYn())
                                .branchNo("-")
                                .deviceNo("-")
                                .branchName("-")
                                .text(notice.getText())
                                .useYn(notice.getUseYn())
                                .startDate(notice.getStartDate())
                                .endDate(notice.getEndDate())
                                .created(notice.getCreated())
                                .build());

                    return deviceRepository.findById(notice.getDevice())
                            .flatMap(device -> branchRepository.findByBranchNo(device.getBranchNo())
                                    .map(branch -> ConciergeNoticeDetailResponseDto.builder()
                                            .branchYn(notice.getBranchYn())
                                            .branchNo(device.getBranchNo())
                                            .deviceNo(device.getDeviceNo())
                                            .branchName(branch.getBranchName())
                                            .text(notice.getText())
                                            .useYn(notice.getUseYn())
                                            .startDate(notice.getStartDate())
                                            .endDate(notice.getEndDate())
                                            .created(notice.getCreated())
                                            .build()));
                })
                .doOnNext(result -> log.info("컨시어지 공지사항을 상세 조회하였습니다."))
                .doOnError(error -> {
                    log.error("컨시어지 공지사항 상세 조회 중 오류가 발생하였습니다.");
                    log.error(error.getMessage());
                });
    }

    public Mono<ConciergeResultResponseDto> modifyConciergeNotice(int id, ConciergeNoticeModifyRequestDto req){
        return conciergeNoticeRepository.findById(id)
                .flatMap(notice -> {
                   if((notice.getUseYn().equals("Y") && req.getUseYn().equals("Y"))
                   || (notice.getUseYn().equals("N") && req.getUseYn().equals("N"))){
                       modifyNoticeForNotice(notice, req);
                       return conciergeNoticeRepository.save(notice);
                   }
                   else if(notice.getUseYn().equals("Y") && req.getUseYn().equals("N"))
                       return conciergeNoticeRepository.findByDeviceAndPositionGreaterThan(notice.getDevice(), notice.getPosition())
                               .flatMap(n -> {
                                  n.setPosition(n.getPosition()-1);
                                  return conciergeNoticeRepository.save(n);
                               })
                               .then(Mono.defer(() -> {
                                   modifyNoticeForNotice(notice, req);
                                   notice.setPosition(-1);
                                   return conciergeNoticeRepository.save(notice);
                               }));

                   return conciergeNoticeRepository.countByDeviceAndBranchYnAndUseYn(notice.getDevice(), notice.getBranchYn(), "Y")
                           .flatMap(count -> {
                               if(notice.getBranchYn().equals("Y") && count>=Common.CONCIERGE_NOTICE_BRANCH_MAX)
                                   return Mono.error(new ConciergeNoticeOverflowException());

                               modifyNoticeForNotice(notice, req);
                               notice.setPosition((int)(count+1));

                               return conciergeNoticeRepository.save(notice);
                           });
                })
                .map(notice -> ConciergeResultResponseDto.builder()
                        .code(0)
                        .message("컨시어지 공지사항 수정: 성공")
                        .build())
                .doOnNext(result -> log.info("컨시어지 공지사항을 수정하였습니다."))
                .onErrorResume(error -> {
                    log.error("컨시어지 공지사항 수정 중 오류가 발생하였습니다.");
                    log.error(error.getMessage());

                    int code;
                    String message;

                    if(error.getClass().getSimpleName().equals("ConciergeNoticeOverflowException")){
                        code = 2;
                        message = "컨시어지 공지사항 수정: 실패(최대 개수 초과)";
                    }
                    else{
                        code = 1;
                        message = "컨시어지 공지사항 수정: 실패(시스템 오류)";
                    }

                    return Mono.just(ConciergeResultResponseDto.builder()
                            .code(code)
                            .message(message)
                            .build());
                });
    }

    public Mono<ConciergeDownloadResponseDto> downloadConciergeGuide(String branchNo, String deviceNo) {
        return deviceRepository.findByBranchNoAndDeviceNo(branchNo, deviceNo)
                .flatMap(device -> conciergeBranchGuideRepository.findByDevice(device.getId())
                        .flatMap(branchGuide -> DataBufferUtils.read(Paths
                                        .get(conciergeGuideFileDirectory+branchGuide.getImage()),
                                new DefaultDataBufferFactory(), Common.CONCIERGE_FILE_BUFFER_SIZE)
                                .map(buffer -> {
                                    byte[] bytes = new byte[buffer.readableByteCount()];
                                    buffer.read(bytes);
                                    DataBufferUtils.release(buffer);
                                    return bytes;
                                })
                                .collectList()
                                .map(bytesList -> bytesList.stream().collect(() -> new ByteArrayOutputStream(),
                                        (os, bytes) -> os.write(bytes, 0, bytes.length),
                                        (os1, os2) -> {}).toByteArray())
                                .map(bytes -> ConciergeDownloadResponseDto.File.builder()
                                        .data(bytes)
                                        .name(branchGuide.getImage())
                                        .build()))
                        .collectList()
                        .map(fileList -> ConciergeDownloadResponseDto.builder()
                                .fileList(fileList)
                                .build())
                        .doOnNext(result -> log.info("컨시어지 지점안내 파일을 다운로드하였습니다."))
                        .doOnError(error -> {
                            log.error("컨시어지 지점안내 파일 다운로드 중 오류가 발생하였습니다.");
                            log.error(error.getMessage());
                        }));
    }

    public Mono<ConciergeBranchGuideGetResponseDto> getConciergeBranchGuide(String branchNo, String deviceNo) {

        return deviceRepository.findByBranchNoAndDeviceNo(branchNo, deviceNo)
                .flatMap(device -> conciergeBranchGuideRepository.findByDevice(device.getId())
                        .flatMap( branchGuide -> conciergeBranchGuideTextRepository.findByConciergeBranchGuide(branchGuide.getId())
                            .map( guideText -> ConciergeBranchGuideGetResponseDto.GuideData.Text.builder()
                                    .title(guideText.getTitle())
                                    .content(guideText.getContent())
                                    .build())
                                .collectList()
                                .map(guideTextList -> ConciergeBranchGuideGetResponseDto.GuideData.builder()
                                .text(guideTextList)
                                .fileName(branchGuide.getImage())
                                .floor(branchGuide.getFloor())
                                .build())
                        ).collectList()
                .map(guideData -> ConciergeBranchGuideGetResponseDto.builder()
                .guideDataList(guideData)
                        .build())
                        .doOnNext(result -> log.info("컨시어지 지점안내를 조회하였습니다."))
                                .doOnError( error -> {
                                    log.error("컨시어지 지점안내 조회 중 오류가 발생했습니다.");
                                    log.error(error.getMessage());
                                }));
    }

    public Mono<ConciergeBranchGuideListResponseDto> listConciergeBranchGuide(int page) {
        return conciergeBranchGuideRepository.findByOrderByCreatedDescId(PageRequest.of(page-1, Common.CONCIERGE_NOTICE_LIST_PER_PAGE))
                .flatMap(branchGuide -> deviceRepository.findById(branchGuide.getDevice())
                        .flatMap(device -> branchRepository.findByBranchNo(device.getBranchNo())
                                .map(branch -> ConciergeBranchGuideListResponseDto.Concierge.builder()
                                    .deviceNo(device.getDeviceNo())
                                    .branchNo(device.getBranchNo())
                                    .branchName(branch.getBranchName())
                                    .deviceId(device.getId())
                                    .created(branchGuide.getCreated())
                                .build()))
                )
                .collectList()
                .map( conciergeList -> ConciergeBranchGuideListResponseDto.builder()
                        .conciergeList(conciergeList)
                        .build())
                .doOnNext(result -> log.info("컨시어지 {} 페이지 지점안내 목록을 조회하였습니다.", page))
                .doOnError(error -> {
                    log.error("컨시어지 지점안내 목록 조회 중 오류가 발생하였습니다.");
                    log.error(error.getMessage());
                });
    }
    public Mono<ConciergePageResponseDto> listPageConciergeGuide(){
        return conciergeBranchGuideRepository.count()
                .map(count -> {
                    int pageCnt = (int)(count/Common.CONCIERGE_NOTICE_LIST_PER_PAGE);

                    if((int)(count%Common.CONCIERGE_NOTICE_LIST_PER_PAGE)!=0)
                        pageCnt += 1;

                    return ConciergePageResponseDto.builder()
                            .pageCnt(pageCnt)
                            .build();
                })
                .doOnNext(result -> log.info("컨시어지 지점안내 목록 페이지 개수를 조회하였습니다."))
                .doOnError(error -> {
                    log.error("컨시어지 지점안내 목록 페이지 개수 조회 중 오류가 발생하였습니다.");
                    log.error(error.getMessage());
                });
    }

    public Mono<ConciergeBranchGuideDetailResponseDto> detailConciergeGuide(int deviceId) {

        return deviceRepository.findById(deviceId)
                .flatMap( device -> branchRepository.findByBranchNo(device.getBranchNo())
                    .flatMap( branch -> conciergeBranchGuideRepository.findByDevice(deviceId)
                        .flatMap( branchGuide -> conciergeBranchGuideTextRepository.findByConciergeBranchGuide(branchGuide.getId())
                            .map( guideText -> ConciergeBranchGuideDetailResponseDto.Guide.Text.builder()
                                .id(guideText.getId())
                                .title(guideText.getTitle())
                                .content(guideText.getContent())
                                .build())
                            .collectList()
                        .map(guideTextList -> ConciergeBranchGuideDetailResponseDto.Guide.builder()
                            .id(branchGuide.getId())
                            .image(branchGuide.getImage())
                            .floor(branchGuide.getFloor())
                            .created(branchGuide.getCreated())
                            .text(guideTextList)
                        .build()))
                    .collectList()
                    .map(guide -> ConciergeBranchGuideDetailResponseDto.builder()
                        .guideList(guide)
                        .deviceNo(device.getDeviceNo())
                        .branchNo(device.getBranchNo())
                        .branchName(branch.getBranchName())
                            .build())
                            .doOnNext(result -> log.info("컨시어지 지점안내 상세를 조회하였습니다."))
                            .doOnError( error -> {
                                log.error("컨시어지 지점안내 상세 조회 중 오류가 발생했습니다.");
                                log.error(error.getMessage());
                            })));
    }
    public Mono<ConciergeResultResponseDto> modifyConciergeGuide (int deviceId, ConciergeBranchGuideModifyRequestDto req){
        return null;
    }


    private Mono<ConciergeResultResponseDto> registerConciergeNotice(ConciergeNoticeSetRequestDto req, int position, Integer device){
        if(req.getUseYn().equals("N")) position = -1;

        return conciergeNoticeRepository.save(ConciergeNotice.builder()
                        .device(device)
                        .text(req.getText())
                        .position(position)
                        .branchYn(req.getBranchYn())
                        .startDate(req.getStartDate())
                        .endDate(req.getEndDate())
                        .useYn(req.getUseYn())
                        .build())
                .doOnNext(result -> {
                    if(device==null) log.info("컨시어지 전체 공지사항이 등록되었습니다.");
                    else log.info("컨시어지 지점({}:{}) 공지사항이 등록되었습니다.", req.getBranchNo(), req.getDeviceNo());
                })
                .map(notice -> ConciergeResultResponseDto.builder()
                        .code(0)
                        .message("컨시어지 공지사항 등록: 성공")
                        .build());
    }

    private Mono<Void> updatePositionAfterDeleteForNotice(ConciergeNotice target){
        return conciergeNoticeRepository
                .findByDeviceAndPositionGreaterThan(target.getDevice(), target.getPosition())
                .flatMap(notice -> {
                    notice.setPosition(notice.getPosition()-1);
                    return conciergeNoticeRepository.save(notice)
                            .doOnNext(result -> log.debug("position({} -> {})으로 변경",
                                    notice.getPosition()+1, notice.getPosition()))
                            .doOnError(error -> {
                                if(target.getDevice()==null) log.error("컨시어지 전체 공지사항 삭제 중 오류가 발생하였습니다.");
                                else log.error("컨시어지 지점 공지사항 삭제 중 오류가 발생하였습니다.");
                                log.error(error.getMessage());
                            });
                }).then();
    }

    private int comparePositionForNotice(ConciergeNotice n1, ConciergeNotice n2){
        if(n1.getPosition()<n2.getPosition()) return -1;
        else if(n1.getPosition()>n2.getPosition()) return 1;
        return 0;
    }

    private void modifyNoticeForNotice(ConciergeNotice notice, ConciergeNoticeModifyRequestDto req){
        notice.setText(req.getText());
        notice.setStartDate(req.getStartDate());
        notice.setEndDate(req.getEndDate());
        notice.setUseYn(req.getUseYn());
    }
}
