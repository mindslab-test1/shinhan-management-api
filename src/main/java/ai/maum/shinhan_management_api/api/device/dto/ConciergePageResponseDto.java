package ai.maum.shinhan_management_api.api.device.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ConciergePageResponseDto {
    private int pageCnt;
}
