package ai.maum.shinhan_management_api.api.device.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ConciergeNoticeGetResponseDto {
    private List<FullNotice> fullNoticeList;
    private List<BranchNotice> branchNoticeList;

    @Data
    @Builder
    public static class FullNotice {
        private String text;
    }

    @Data
    @Builder
    public static class BranchNotice {
        private String text;
    }
}
