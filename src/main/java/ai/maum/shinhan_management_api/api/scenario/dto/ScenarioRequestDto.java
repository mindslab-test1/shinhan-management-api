package ai.maum.shinhan_management_api.api.scenario.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder
public class ScenarioRequestDto {
}
