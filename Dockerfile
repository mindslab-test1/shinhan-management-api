FROM adoptopenjdk/openjdk11:alpine-jre

ARG DEPENDENCY=build/libs
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} app.jar
ENV ACTIVE_PROFILE dev
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=${ACTIVE_PROFILE}","/app.jar"]
